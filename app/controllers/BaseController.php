<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

    public $themeAdmin;



    public function __construct() {

        $this->themeAdmin = Theme::uses('admin')->layout('default');

    }

    public function statusUpdate($data){
        $status=ENTREP;
        return $status;
    }

    public function recursive($data) {

            try{

                $usersLevel = DB::table('tbl_user')
                    ->where('user_id',$data['user_id'])
                    ->first();

                /******update points******/
                $arr =array(
                  'new_dealer'                  => $data['new_dealer'],
                  'package_type'                =>   $usersLevel->user_package_type,
                  'placement_id'                =>   $usersLevel->user_placement,
                  'placement_position'          =>   $data['position'],
                  'user_id'                     =>   $usersLevel->user_id,
                  'user_total_points_left'      =>   $usersLevel->user_total_points_left,
                  'user_total_points_right'     =>   $usersLevel->user_total_points_right,
                  'new_recruit_package'         =>   $data['new_recruit_package'],
                  'packageLowestPoints'         =>   $data['packageLowestPoints'],
                  'packageLowestPairingEarn'    =>   $data['packageLowestPairingEarn']
                );


                $placement = DB::table('tbl_user')->where('user_dealer_id',$usersLevel->user_placement)->first();
                if($usersLevel->user_level==1 ){

                    $this->earningsComputation($arr);
                }else if($usersLevel->user_level>1){
                    $this->earningsComputation($arr);

                }

                $ar = array(
                    'new_dealer'                => $data['new_dealer'],
                    'new_recruit_package'   => $data['new_recruit_package'],
                    'user_id'               => $placement->user_id,
                    'position'              => $usersLevel->user_placement_position,
                    'packageLowestPoints'       => $data['packageLowestPoints'],
                    'packageLowestPairingEarn'  => $data['packageLowestPairingEarn']
                );

                return $this->recursive($ar);
            }catch(Exception $ex){

                $usersLevel = DB::table('tbl_user')
                    ->where('user_id',$data['user_id'])
                    ->first();

                /******update points******/
                $arr =array(
                    'new_dealer'                  =>   $data['new_dealer'],
                    'package_type'                =>   $usersLevel->user_package_type,
                    'placement_id'                =>   $usersLevel->user_placement,
                    'placement_position'          =>   $data['position'],
                    'user_id'                     =>   $usersLevel->user_id,
                    'user_total_points_left'      =>   $usersLevel->user_total_points_left,
                    'user_total_points_right'     =>   $usersLevel->user_total_points_right,
                    'new_recruit_package'         =>   $data['new_recruit_package'],
                    'packageLowestPoints'         =>   $data['packageLowestPoints'],
                    'packageLowestPairingEarn'    =>   $data['packageLowestPairingEarn']
                );

                $this->earningsComputation($arr);

            }
    }

    public function earningsComputation($data){

        /***check the placement dealer package type***/
            $package_type           =   $data['package_type'];
            $packageData            =   $this->packageModel()->selectPackageByPackageId($package_type);
            $newRecruitPackage      =   $this->packageModel()->selectPackageByPackageId($data['new_recruit_package']);
            $earnPoints             =   $newRecruitPackage->package_points;

            $arr = array(
                'new_dealer'                => $data['new_dealer'],
                'placement_position'        =>$data['placement_position'],
                'dealer_id'                 =>$data['user_id'],
                'points'                    =>$earnPoints,
                'current_left_points'       =>$data['user_total_points_left'],
                'current_right_points'      =>$data['user_total_points_right'],
                'dealer_package_points'     => $packageData->package_points,
                'package_pairing_earn'      => $packageData->package_pairing_earn
            );
        /********Save Earned Points*********/
            $this->userEarnPointsModel()->saveEarnPoints($arr);

            $d=$this->userPairingPointsModel()->selectByDealerLimitOne($data['user_id']);
            $result=$this->userPairingPointsModel()->selectByDealerAndCurrentDate($data['user_id']);
            $pairingPoints = 0;
            try{
                foreach($result as $val){
                    if($val->left_points <= $val->right_points){
                        $paired_points = $val->left_points;
                    }else if($val->right_points <= $val->left_points){
                        $paired_points = $val->right_points;
                    }
                    $pairingPoints = $pairingPoints+$paired_points;

                }
            }catch(Exception $ex){

            }
            if(($pairingPoints/2)!=PAIRING_LIMIT){
                if($d){
                    $ar = array(
                        'id' =>$d->id,
                        'points' =>$earnPoints,
                        'placement_position' =>$data['placement_position'],
                    );
                    if($data['placement_position']==LEFT){

                            if($d->left_points==0){
                                $this->userPairingPointsModel()->updatePairingLeftPoints($ar);
                                if($earnPoints>$d->right_points){
                                    $position=LEFT;
                                }else{
                                    $position=RIGHT;
                                }
                                 $dtArray = array(
                                     'points_position' =>$d->right_points,
                                     'position_points' =>$position,
                                     'earnPoints' =>$earnPoints,
                                     'user_id' =>$data['user_id'],
                                     'packageLowestPoints' =>$data['packageLowestPoints'],
                                     'packageLowestPairingEarn' =>$data['packageLowestPairingEarn']
                                 );
                                $this->pairingComputation($dtArray,$arr);

                            }else{
                                if($d->right_points!=0 && $d->left_points!=0){
                                    $this->userPairingPointsModel()->savePairingPoints($arr);
                                }else{
                                    $this->userPairingPointsModel()->updatePairingPoints($ar,$d->left_points);
                                }
                            }
                    }else{

                        if($d->right_points==0){

                            $this->userPairingPointsModel()->updatePairingRightPoints($ar);
                            if($earnPoints>$d->left_points){
                                $position=RIGHT;
                            }else{
                                $position=LEFT;
                            }
                            $dtArray = array(
                                'points_position' =>$d->left_points,
                                'position_points' =>$position,
                                'earnPoints' =>$earnPoints,
                                'user_id' =>$data['user_id'],
                                'packageLowestPoints' =>$data['packageLowestPoints'],
                                'packageLowestPairingEarn' =>$data['packageLowestPairingEarn']
                            );


                             $this->pairingComputation($dtArray,$arr);
                        }else{
                            if($d->right_points!=0 && $d->left_points!=0){
                                $this->userPairingPointsModel()->savePairingPoints($arr);
                            }else{
                                $this->userPairingPointsModel()->updatePairingPoints($ar,$d->right_points);
                            }

                        }
                    }

                }else{
                    $this->userPairingPointsModel()->savePairingPoints($arr);
                }
            }
        /***Update User Total Points Left and Right***/
            $this->userModel()->updateUserTotalPoints($arr);

        /***Check If User Left Points and Right Points are Pair***/

    }

    public function pairingComputation($data,$arr){

        $diff = $data['points_position']-$data['earnPoints'];

        $arr['placement_position']=$data['position_points'];
        if($diff<0){
            $pointsPaired   = $diff * -1;
            $arr['points']=$pointsPaired;
            $pointPosition = $data['points_position']-$pointsPaired;
            $this->userPairingPointsModel()->savePairingPoints($arr);

            /***Compute Earning By Points and Save Earnings***/
            $pointsEarnedQty= $data['points_position']/$data['packageLowestPoints'];
            $totalEarned =  $pointsEarnedQty*$data['packageLowestPairingEarn'];
            //$pairingPoints= $pointsEarnedQty*$data['packageLowestPoints'];
            //if($pairingPoints<0){$pairingPoints=$pairingPoints*-1;}
            //if($pairingPoints==$arr['dealer_package_points']){
              //  $totalEarned =  $arr['package_pairing_earn'];
            //}

            $foo=array(
                'userId'    => $data['user_id'],
                'earnAmount'  => $totalEarned,
            );

            $this->userEarningsModel()->saveEarning($foo);
            /***update dealer earnings in tbl_user**/

             $userDetail=$this->userModel()->selectUserByUserId($data['user_id']);
             $currentEarnedUser = $userDetail->user_total_earnings;
             $userTotalEarned = $currentEarnedUser + $totalEarned;
             $dataArray = array(
                'userId' =>$data['user_id'],
                'totalEarned' =>$userTotalEarned
            );
            $this->userModel()->updateUserEarning($dataArray);

        }else if($diff>0){

            $pointsPaired  = $diff;
            $arr['points']=$pointsPaired;
            $pointPosition = $data['points_position']-$pointsPaired;
            $this->userPairingPointsModel()->savePairingPoints($arr);

            /***Compute Earning By Points and Save Earnings***/
            $pointsEarnedQty= $pointPosition/$data['packageLowestPoints'];
            $totalEarned =  $pointsEarnedQty*$data['packageLowestPairingEarn'];
            $pairingPoints= $pointsEarnedQty*$data['packageLowestPoints'];
            if($pairingPoints==$arr['dealer_package_points']){
                $totalEarned =  $arr['package_pairing_earn'];
            }

            $foo=array(
                'userId'    => $data['user_id'],
                'earnAmount'  => $totalEarned,
            );
            $this->userEarningsModel()->saveEarning($foo);

            /***update dealer earnings in tbl_user**/
            $userDetail=$this->userModel()->selectUserByUserId($data['user_id']);
            $currentEarnedUser = $userDetail->user_total_earnings;
            $userTotalEarned = $currentEarnedUser + $totalEarned;
            $dataArray = array(
                'userId' =>$data['user_id'],
                'totalEarned' =>$userTotalEarned
            );
            $this->userModel()->updateUserEarning($dataArray);
        }else{

            /***Compute Earning By Points and Save Earnings***/
            $pointsEarnedQty=$data['points_position']/$data['packageLowestPoints'];
            $totalEarned =  $pointsEarnedQty*$data['packageLowestPairingEarn'];

            if($arr['dealer_package_points']==SILVER_POINTS){
                $totalEarned =  $arr['package_pairing_earn'];
            }else if($arr['dealer_package_points']==BRONZE_POINTS){
                // $totalEarned =  $arr['package_pairing_earn'];
            }

            $foo=array(
                'userId'    => $data['user_id'],
                'earnAmount'  => $totalEarned,
            );
            $this->userEarningsModel()->saveEarning($foo);

            /***update dealer earnings in tbl_user**/
            $userDetail=$this->userModel()->selectUserByUserId($data['user_id']);

            $currentEarnedUser = $userDetail->user_total_earnings;
            $userTotalEarned = $currentEarnedUser + $totalEarned;
            $dataArray = array(
                'userId' =>$data['user_id'],
                'totalEarned' =>$userTotalEarned
            );
            $this->userModel()->updateUserEarning($dataArray);

        }



    }

    public function payInCodeModel(){
        $model = new PayInCode();
        return $model;
    }
    public function packageModel(){
        $model = new Package();
        return $model;
    }
    public function userModel()
    {
        $model = new User();
        return $model;
    }
    public function registrationModel()
    {
        $modelRegistration = new Registration();
        return $modelRegistration;
    }
    public function userDownlineModel(){
        $model =  new UserDownline();
        return $model;
    }
    public function userEarningsModel(){
        $model = new UserEarnings();
        return $model;
    }
    public function userWithdrawModel(){
        $model  = new UserWithdraw();
        return$model;
    }
    public function userEarnPointsModel(){
        $model  = new UserEarnPoints();
        return$model;
    }
    public function userPairingPointsModel(){
        $model  = new UserPairingPoints();
        return$model;
    }
    public function companyModel(){
        $model = new Company();
        return $model;
    }
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
    public function setDatatable($cQryObj, $aColumns = array(), $sIndexColumn = "")
    {
        $iDisplayStart   = Input::get('iDisplayStart');
        $iDisplayLength = Input::get('iDisplayLength');
        $iSortCol   = Input::get('iSortCol_0');
        $iSortingCols = Input::get('iSortingCols');
        $sSearch    = Input::get('sSearch');

        /* LIMIT */
        $sLimit = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
            $sLimit = $iDisplayStart.", ".$iDisplayLength;
        }

        /* SORT */
        if (isset($iSortCol)) {
            $sOrder = "";
            for ($i=0; $i < intval($iSortingCols); $i++) {
                if (Input::get('bSortable_'.intval(Input::get('iSortCol_'.$i))) == "true") {
                    $sOrder .= $aColumns[intval(Input::get('iSortCol_'.$i))]."*".Input::get('sSortDir_'.$i).',';
                }
            }
        }

        /* WHERE */
        $sWhere = "";
        if ($sSearch != "") {
            for ($i=0; $i < count($aColumns); $i++) {
                if(isset($aColumns[$i]) && !empty($aColumns[$i])) {
                    $sWhere .= $aColumns[$i]."*".$sSearch."|";
                }
            }
        }

        for ($i=0; $i < count($aColumns); $i++) {
            if(isset($aColumns[$i]) && !empty($aColumns[$i])) {
                if (Input::get('bSearchable_'.$i) == "true" && Input::get('sSearch_'.$i) != '') {
                    if ($sWhere == "") {
                        $sWhere = "WHERE ";
                    } else {
                        $sWhere = "AND ";
                    }
                    $sWhere .= $aColumns[$i].", ".Input::get('sSearch_'.$i);
                }
            }
        }

        $order_by   = explode(",", $sOrder);
        $limits     = explode(",", $sLimit);
        $filter     = explode("|", $sWhere);

        $cQryObjOrig = clone $cQryObj;
        $cQryObjTemp = clone $cQryObj;
        $cQryObjTemp = $cQryObjTemp->take($limits[1])->skip($limits[0]);

        /*if ($sWhere != "") {
            for ($i=0; $i < count($filter) -1; $i++) {
                $xFilter = explode("*", $filter[$i]);
                if($i == 0) {
                    $cQryObjTemp = $cQryObjTemp->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%');
                    $cQryObjOrig = $cQryObjOrig->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%');
                } else {
                    $cQryObjTemp = $cQryObjTemp->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%', 'OR');
                    $cQryObjOrig = $cQryObjOrig->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%', 'OR');
                }
            }
        }*/

        if ($sWhere != "") {
            $cQryObjTemp->where(function($query) use ($i, $filter, $cQryObjTemp, $cQryObjOrig) {
                for ($i=0; $i < count($filter) -1; $i++) {
                    $xFilter = explode("*", $filter[$i]);
                    if($i == 0) {
                        $cQryObjTemp = $query->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%');
                        $cQryObjOrig = $query->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%');
                    } else {
                        $cQryObjTemp = $query->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%', 'OR');
                        $cQryObjOrig = $query->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%', 'OR');
                    }
                }
            });
        }

        if ($sOrder != "") {
            for ($i=0; $i < count($order_by) -1; $i++) {
                $xOrder = explode("*", $order_by[$i]);
                if($xOrder[0]) {
                    $cQryObjTemp = $cQryObjTemp->orderBy($xOrder[0], $xOrder[1]);
                    $cQryObjOrig = $cQryObjOrig->orderBy($xOrder[0], $xOrder[1]);
                }
            }
        }

        /* OUTPUT DATA */
        $cQryObjResult  = $cQryObjTemp->get();
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        // *var_dump($last_query);

        $output = array(
            "sEcho"                 => intval(Input::get('sEcho')),
            "iTotalRecords"         => count($cQryObjOrig->get()),
            "iTotalDisplayRecords"  => count($cQryObjOrig->get()),
            "aaData"                => array(),
            'objResult'             => $cQryObjResult,
        );

        return $output;
    }
}
