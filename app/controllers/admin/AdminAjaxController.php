<?php

class AdminAjaxController extends BaseController {


    public function ajax()
    {
        $type = Input::get('type');
        $return = array();
        switch($type)
        {

            case 'pay_in': {

                $transaction = DB::table('tbl_pay_in_code');

                $dtResult = GlobalFunctionController::setDatatable($transaction, array(
                    'pay_in_id',
                    'pay_in_code',
                    'pay_in_date',
                    'pay_in_user',
                    'pay_in_status'
                    ),
                    'pay_in_id');

                foreach ($dtResult['objResult'] as $aRow){



                    $data = array(
                        $aRow->pay_in_id,
                        $aRow->pay_in_code,
                        $aRow->pay_in_date,
                        $aRow->pay_in_user,
                        $aRow->pay_in_status
                    );

                    $dtResult['aaData'][] = $data;
                }

                unset($dtResult['objResult']);
                echo json_encode($dtResult);
                break;

            }

            case 'generate_code':{

                $noOfRequest=Input::get('qty');
                $packageType=intval(Input::get('package'));
                $randomUnique=GlobalFunctionController::MakeUnique(8,$noOfRequest);

                $mytime = Carbon\Carbon::now();

                $data=array(
                    'date' => $mytime->toDateTimeString(),//$current_date->format('Y-m-d H:i:s'),
                    'gencode'=> $randomUnique,
                    'user'   => Auth::user()->user_fname.' '.Auth::user()->user_lname
                );

                $idArray=array();
                foreach($randomUnique as $key){
                    $securityCodeUnique = GlobalFunctionController::makeSecurityCodeUnique(9);
                    $dt=array(
                        'code' => $key,
                        'security' => $securityCodeUnique,
                        'date' => $data['date'],
                        'user' => Auth::user()->user_id,
                        'status' => 1,
                        'package' => $packageType
                    );
                    $id=$this->payInCodeModel()->savePayIncode($dt);
                    array_push($idArray,$id);
                }
                $dataArr=array();
                foreach($idArray as $id){
                    $result=$this->payInCodeModel()->selectCodePerId($id);
                    array_push($dataArr,$result);
                }

                return Response::json(
                    array(
                        'result' => $dataArr
                    )
                );
                break;
            }

            case 'search_code':{

                $noOfRequest=Input::get('qty');
//                $randomUnique=GlobalFunctionController::MakeUnique(8,$noOfRequest);
//
//                $current_date = new DateTime();
//                $data=array(
//                    'date' => $current_date->format('Y-m-d H:i:s'),
//                    'gencode'=> $randomUnique,
//                    'user'   => Auth::user()->user_fname.' '.Auth::user()->user_lname
//                );
//                $idArray=array();
//                foreach($randomUnique as $key){
//                    $securityCodeUnique = GlobalFunctionController::makeSecurityCodeUnique(9);
//                    $dt=array(
//                        'code' => $key,
//                        'security' => $securityCodeUnique,
//                        'date' => $data['date'],
//                        'user' => Auth::user()->user_id,
//                        'status' => 1,
//                    );
//                    $id=$this->payInCodeModel()->savePayIncode($dt);
//                    array_push($idArray,$id);
//                }
//                $dataArr=array();
//                foreach($idArray as $id){
//                    $result=$this->payInCodeModel()->selectCodePerId($id);
//                    array_push($dataArr,$result);
//                }
                $dataArr=$this->payInCodeModel()->lastCodeGeneratedByCount($noOfRequest);

                return Response::json(
                    array(
                        'result' => $dataArr
                    )
                );
                break;
            }
            case 'payout-list':{
                $tbl_user_withdraw=new UserWithdraw();
                $transaction=$tbl_user_withdraw->getPayoutList();

                $dtResult = $this->setDatatable($transaction, array(
                    'user_dealer_id',
                    'user_lname',
                    'user_fname',
                    'user_mname',
                    'user_withdraw_amount',
                    'user_withdraw_date',
                    'user_withdraw_id',
                    'user_withdraw_id'
                ),
                'user_withdraw_id');

                foreach ($dtResult['objResult'] as $aRow){
                    if($aRow->user_withdraw_type==0) {
                        $type = 'Cash';
                    }elseif($aRow->user_withdraw_type==1){
                        $type = 'Bank';
                    }else{
                        $type = 'Check';
                    }
                    $data = array(
                        $aRow->user_dealer_id,
                        $aRow->user_lname . ', ' . $aRow->user_fname . ', ' . $aRow->user_mname,
                        $aRow->user_withdraw_amount,
                        $aRow->user_withdraw_date,
                        $type,
                        '<a href="#" data-type="'.$aRow->user_withdraw_type.'" data-id="'.$aRow->user_withdraw_id.'" data-href="'.$aRow->user_withdraw_id.'" data-bank="'.''.'" data-account-no="'.$aRow->user_withdraw_account.'" data-account-name="'.$aRow->user_withdraw_account_name.'" data-toggle="modal" data-target="#confirm-cashout">Cashout</a>'
                    );
                    $dtResult['aaData'][] = $data;
                }

                unset($dtResult['objResult']);
                echo json_encode($dtResult);
                break;
            }
        }
    }




}

?>