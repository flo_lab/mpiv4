<?php
/**
 * Created by PhpStorm.
 * User: larry
 * Date: 10/8/2015
 * Time: 6:50 PM
 */

class AdminPanelController extends  BaseController{

    public function logout(){

        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }
    public function admin(){
        return $this->themeAdmin->of('admin.index',array('Status' => ''))->render();
    }

    public function payin(){
        $package =DB::table("tbl_package")->get();
        return $this->themeAdmin->of('admin.payin',array('Status' => '','Package'=>$package))->render();
    }
    public function member(){
        return $this->themeAdmin->of('admin.member',array('Status' => ''))->render();
    }
    public function payout(){
        return $this->themeAdmin->of('admin.payout',array('Status' => ''))->render();
    }
    public function transaction(){
        return $this->themeAdmin->of('admin.transaction',array('Status' => ''))->render();
    }
    public function transactionlog(){
        return $this->themeAdmin->of('admin.transactionlog',array('Status' => ''))->render();
    }
    public function adminCashout(){
        $user_withdraw_id=Input::get('id');
        $user_withdraw_user=Input::get('userId');

       DB::transaction(function() use($user_withdraw_id,$user_withdraw_user){
           $tbl_user_withdraw=new UserWithdraw();
           $tbl_user=new User();
           $tbl_user_withdraw->updateStatus($user_withdraw_id);
           $user_withdraw_amount=$tbl_user_withdraw->getAmount($user_withdraw_id);
           $withdraw_amount=$user_withdraw_amount->user_withdraw_amount;
           $user_total_earnings=$tbl_user->getTotalEarnings($user_withdraw_user);
           $user_earnings=$user_total_earnings->user_total_earnings;

           $update_amount=floatval($user_earnings) - floatval($withdraw_amount);
           $tbl_user->updateTotalEarnings($user_withdraw_user,$update_amount);
        });

    }
}