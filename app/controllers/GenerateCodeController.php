<?php
/**
 * Created by PhpStorm.
 * User: larry
 * Date: 10/8/2015
 * Time: 6:50 PM
 */

class GenerateCodeController extends BaseController{

//    function MakeUnique($length,$cnt) {
//        $salt       = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
//        $len        = strlen($salt);
//        $arr=[];
//        for($x=1;$x<=$cnt;$x++){
//            $makepass   = '';
//            mt_srand(10000000*(double)microtime());
//            for ($i = 0; $i < $length; $i++) {
//                $makepass .= $salt[mt_rand(0,$len - 1)];
//            }
//            if($x<=9){
//                array_push($arr,$makepass.$x);
//            }else if($x<=99){
//                $var=substr($makepass,0,7);
//                array_push($arr,$var.$x);
//            }elseif($x<=999){
//                $var=substr($makepass,0,6);
//                array_push($arr,$var.$x);
//            }
//
//       }
//
//        return $arr;
//    }


//    function makeSecurityCodeUnique($length) {
//
//        $countPayIn = DB::table('tbl_pay_in_code')->count();
//        $salt       = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".$countPayIn;
//        $charactersLength        = strlen($salt);
//        $randomString = '';
//        for ($i = 0; $i < $length; $i++) {
//            $randomString .= $salt[rand(0, $charactersLength - 1)];
//        }
//
//        return $randomString;
//    }
//

    public function generateCode(){

//        $noOfRequest=Input::get('qty');
//        $randomUnique=$this->MakeUnique(8,$noOfRequest);
//
//        $current_date = new DateTime();
//        $data=array(
//            'date' => $current_date->format('Y-m-d H:i:s'),
//            'gencode'=> $randomUnique,
//            'user'   => Auth::user()->user_fname.' '.Auth::user()->user_lname
//        );
//        $idArray=array();
//        foreach($randomUnique as $key){
//            $securityCodeUnique = $this->makeSecurityCodeUnique(9);
//            $dt=array(
//                'code' => $key,
//                'security' => $securityCodeUnique,
//                'date' => $data['date'],
//                'user' => Auth::user()->user_id,
//                'status' => 1,
//            );
//            $id=$this->payInCodeModel()->savePayIncode($dt);
//            array_push($idArray,$id);
//        }
//        $dataArr=array();
//        foreach($idArray as $id){
//            $result=$this->payInCodeModel()->selectCodePerId($id);
//            array_push($dataArr,$result);
//        }
//
//        return Response::json(
//            array(
//                'result' => $dataArr
//            )
//        );


}

    public function printTableCode(){
        $id = Input::get('id');
        $code = Input::get('code');
        $security = Input::get('security');
        $user = Input::get('user');
        $date = Input::get('date');
        $status = Input::get('status');
        $package = Input::get('package');

        $arrayId = explode(",",$id[0]);
        $arrayCode = explode(",",$code[0]);
        $arraySecCode = explode(",",$security[0]);
        $arrayUser = explode(",",$user[0]);
        $arrayDate = explode(",",$date[0]);
        $arrayStatus = explode(",",$status[0]);
        $arrayPackage = explode(",",$package[0]);
        $da =array();

       for($i=0;$i<count($arrayId);$i++){
           $user = User::find($arrayUser[$i]);

           $arr= array(
               'id'=>$arrayId[$i],
               'code'=>$arrayCode[$i],
               'security'=>$arraySecCode[$i],
               'user'=>$user->user_fname.' '.$user->user_lname,
               'date'=>$arrayDate[$i],
               'status'=>$arrayStatus[$i],
               'package'=>$arrayPackage[$i]
           );
           array_push($da,$arr);
       }
        //$json_decoded       = json_decode(Input::get('jsonItem'));
//        $arrayCode = array();
//        foreach ($json_decoded->codeGen as $val)
//        {
//            $user = User::find($val->user);
//            $dt=array(
//                'id' => $val->id,
//                'code' => $val->code,
//                'date' => $val->date,
//                'user' => $user->user_fname.' '.$user->user_lname
//
//            );
//
//            array_push($arrayCode,$dt);
//        }
//        $d=array('gencode'=>$arrayCode);

        $pdf = PDF::loadView('admin.generatecode',array('gencode'=>$da));
        return $pdf->stream();

    }

}