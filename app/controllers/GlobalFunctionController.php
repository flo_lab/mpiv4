<?php


class GlobalFunctionController extends BaseController{


    /**@ function for loop of downline**/
    public static function getDownLineInfo($data){
        $result =BaseController::userModel()->selectUserByDealer($data);
        return $result;
    }

    /**@ function generate unique seciruty code**/
    public static function MakeUnique($length,$cnt) {
        $salt       = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $len        = strlen($salt);
        $arr=[];
        for($x=1;$x<=$cnt;$x++){
            $makepass   = '';
            mt_srand(10000000*(double)microtime());
            for ($i = 0; $i < $length; $i++) {
                $makepass .= $salt[mt_rand(0,$len - 1)];
            }
            if($x<=9){
                array_push($arr,$makepass.$x);
            }else if($x<=99){
                $var=substr($makepass,0,7);
                array_push($arr,$var.$x);
            }elseif($x<=999){
                $var=substr($makepass,0,6);
                array_push($arr,$var.$x);
            }

        }

        return $arr;
    }
    public static function makeSecurityCodeUnique($length) {

        $countPayIn = DB::table('tbl_pay_in_code')->count();
        $salt       = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".$countPayIn;
        $charactersLength        = strlen($salt);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $salt[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**@ function transLog record audit trail*/
    public static function transLog($data)
    {

        //  for audit trail
        $currentdate = new DateTime();
        $brTrmnlModel= new BranchTerminal();
        $ip = self::getRealIpAddrClient();
        $transLogModel = new TransactionLog();
        if($ip['macClient']==false){

            $mac= strtoupper($ip['mac']);

        } else {

            $mac= strtoupper($ip['mac']);
        }

        try {

            $terminal = $brTrmnlModel->selectTerminal($mac);
            $trmnl=$terminal->brTrmnl_id;
            $branch=$terminal->brTrmnl_brnch;
        } catch (Exception $ex) {
            $trmnl=0;
            if($data['module']==ADMIN){
                $branch=$data['branch'];
            } else {
                $branch=0;
            }

        }

        $transLogData = array(
            'user'         =>  $data['user'],
            'terminal'     =>  $trmnl,
            'module'       =>  $data['module'],
            'transaction'  =>  $data['transac'],
            'transDesc'    => '( '.$data['type'].' )  User '. $data['transac'].$data['order'].$data['orderNum'].$data['withReceipt'].$data['receipt'],
            'transDate'    =>  $currentdate,
            'branch'       =>  $branch
        );
        $transLogModel->saveTransLog($transLogData);
        // end audit trail
    }

    public static function encryptPassword($password)
    {
        $cryptKey  = 'skjAK23jfKAj23i9850ASjkD';
        $qEncoded  = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $password, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
        return( $qEncoded );
    }

//    public static function setDatatable($cQryObj, $aColumns = array(), $sIndexColumn = "")
//    {
//        $iDisplayStart   = Input::get('iDisplayStart');
//        $iDisplayLength = Input::get('iDisplayLength');
//        $iSortCol   = Input::get('iSortCol_0');
//        $iSortingCols = Input::get('iSortingCols');
//        $sSearch    = Input::get('sSearch');
//
//        /* LIMIT */
//        $sLimit = "";
//        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
//            $sLimit = $iDisplayStart.", ".$iDisplayLength;
//        }
//
//        /* SORT */
//        if (isset($iSortCol)) {
//            $sOrder = "";
//            for ($i=0; $i < intval($iSortingCols); $i++) {
//                if (Input::get('bSortable_'.intval(Input::get('iSortCol_'.$i))) == "true") {
//                    $sOrder .= $aColumns[intval(Input::get('iSortCol_'.$i))]."*".Input::get('sSortDir_'.$i).',';
//                }
//            }
//        }
//
//        /* WHERE */
//        $sWhere = "";
//        if ($sSearch != "") {
//            for ($i=0; $i < count($aColumns); $i++) {
//                if(isset($aColumns[$i]) && !empty($aColumns[$i])) {
//                    $sWhere .= $aColumns[$i]."*".$sSearch."|";
//                }
//            }
//        }
//
//        for ($i=0; $i < count($aColumns); $i++) {
//            if(isset($aColumns[$i]) && !empty($aColumns[$i])) {
//                if (Input::get('bSearchable_'.$i) == "true" && Input::get('sSearch_'.$i) != '') {
//                    if ($sWhere == "") {
//                        $sWhere = "WHERE ";
//                    } else {
//                        $sWhere = "AND ";
//                    }
//                    $sWhere .= $aColumns[$i].", ".Input::get('sSearch_'.$i);
//                }
//            }
//        }
//
//        $order_by   = explode(",", $sOrder);
//        $limits     = explode(",", $sLimit);
//        $filter     = explode("|", $sWhere);
//
//        $cQryObjOrig = clone $cQryObj;
//        $cQryObjTemp = clone $cQryObj;
//        $cQryObjTemp = $cQryObjTemp->take($limits[1])->skip($limits[0]);
//
//        /*if ($sWhere != "") {
//            for ($i=0; $i < count($filter) -1; $i++) {
//                $xFilter = explode("*", $filter[$i]);
//                if($i == 0) {
//                    $cQryObjTemp = $cQryObjTemp->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%');
//                    $cQryObjOrig = $cQryObjOrig->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%');
//                } else {
//                    $cQryObjTemp = $cQryObjTemp->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%', 'OR');
//                    $cQryObjOrig = $cQryObjOrig->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%', 'OR');
//                }
//            }
//        }*/
//
//        if ($sWhere != "") {
//            $cQryObjTemp->where(function($query) use ($i, $filter, $cQryObjTemp, $cQryObjOrig) {
//                for ($i=0; $i < count($filter) -1; $i++) {
//                    $xFilter = explode("*", $filter[$i]);
//                    if($i == 0) {
//                        $cQryObjTemp = $query->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%');
//                        $cQryObjOrig = $query->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%');
//                    } else {
//                        $cQryObjTemp = $query->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%', 'OR');
//                        $cQryObjOrig = $query->where($xFilter[0], 'LIKE', '%'.$xFilter[1].'%', 'OR');
//                    }
//                }
//            });
//        }
//
//        if ($sOrder != "") {
//            for ($i=0; $i < count($order_by) -1; $i++) {
//                $xOrder = explode("*", $order_by[$i]);
//                if($xOrder[0]) {
//                    $cQryObjTemp = $cQryObjTemp->orderBy($xOrder[0], $xOrder[1]);
//                    $cQryObjOrig = $cQryObjOrig->orderBy($xOrder[0], $xOrder[1]);
//                }
//            }
//        }
//
//        /* OUTPUT DATA */
//        $cQryObjResult  = $cQryObjTemp->get();
//        $queries = DB::getQueryLog();
//        $last_query = end($queries);
//        // *var_dump($last_query);
//
//        $output = array(
//            "sEcho"                 => intval(Input::get('sEcho')),
//            "iTotalRecords"         => count($cQryObjOrig->get()),
//            "iTotalDisplayRecords"  => count($cQryObjOrig->get()),
//            "aaData"                => array(),
//            'objResult'             => $cQryObjResult,
//        );
//
//        return $output;
//    }
}