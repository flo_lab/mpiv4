<?php
/**
 * Created by PhpStorm.
 * User: Arjay
 * Date: 4/9/2016
 * Time: 9:55 AM
 */
class ForgotPasswordController extends BaseController {

    public $theme;
    public function __construct() {
        $this->theme = Theme::uses('mpi')->layout('default');
    }

    public function index(){
        return $this->theme->of('mpi.forgot-password')->render();
    }

    public function emailChangePassword(){

        $data = array(
            'email' => Input::get('email'),
            'username' => Input::get('username'),

        );

        $rules = array(
            'email'     => 'required|exists:tbl_user,user_email',
            'username'  => 'required|exists:tbl_user,user_username'
        );


        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {

            return Redirect::to('forgot-password')
                ->withErrors($validator)
                ->withInput(Input::except('password'));

        } else {

            $result = $this->userModel()->selectDealerByEmail($data['email']);

            if($result){

                $company=$this->companyModel()->selectCompanyFirst();
                $emailFrom=$company->tbl_company_email;
                $encryptedParam = GlobalFunctionController::encryptPassword($result->user_username);
                $securityCodeUnique = GlobalFunctionController::makeSecurityCodeUnique(9);

                return DB::transaction(function() use(
                    $encryptedParam,$result,
                    $securityCodeUnique,
                    $data,$emailFrom,$company
                ){


                    $_data =array(

                        'encryptedParam'           =>   str_replace('/','',$encryptedParam.$securityCodeUnique),
                        'emailTo'           =>   $data['email'],
                        'emailFrom'         =>   $emailFrom,
                        'emailSubject'      =>   $company->tbl_company_name.' Change Password',
                        'id'                =>    $result->user_id
                    );
                    $this->userModel()->saveChangePasswordTokenById($_data);

                    /**send email to customer**/
                    Mail::send('emailNotification',$_data,
                        function($message) use ($_data) {
                            $message->from($_data['emailFrom']);
                            $message->to($_data['emailTo'])->subject($_data['emailSubject']);
                        });
                    /**end of email function**/

                    return Redirect::to('forgot-password')
                        ->with('Status','Success');
                });


            }else{

                return Redirect::to('forgot-password')
                    ->with('Status','Email is not existed');

            }
        }

    }

    public function changePassword($email,$username){

        return $this->theme->of('mpi.new-password',array('email' => $email,'username' => $username))->render();
    }

    public function changePasswordProcess(){
        $data = array(
            'email'             => Input::get('email'),
            'username'          => Input::get('username'),
            'new-password'      => Input::get('new-password'),
            'retype-password'   => Input::get('retype-password')

        );

        $rules = array(
            'email'     => 'required|exists:tbl_user,user_email',
            'username'  => 'required',
            'new-password'  => 'required',
            'retype-password'  => 'required'
        );


        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {

            return Redirect::to('change-password/'.$data['email'].'/'.$data['username'])
                ->withErrors($validator)
                ->withInput(Input::except('password'));

        } else {

            if($data['new-password']==$data['retype-password']){
                return DB::transaction(function() use($data){

                    $result = $this->userModel()->selectDealerByEmail($data['email']);
                    $foo = array(
                        'id'=>$result->user_id,
                        'new-password'=>$data['new-password']
                    );
                    if($result->user_change_password_token==$data['username']){
                        $this->userModel()->updatePasswordById($foo);
                        $this->userModel()->removePasswordTokenById($foo);

                        return Redirect::to('change-password/'.$data['email'].'/'.$data['username'])
                            ->with('Status','Success');
                    }else{
                        return Redirect::to('change-password/'.$data['email'].'/'.$data['username'])
                            ->with('Status','Email token is not valid or already used.');
                    }


                });


            }else{

                return Redirect::to('change-password/'.$data['email'].'/'.$data['username'])
                    ->with('Status','Password do not match');

            }
        }

    }




}


?>