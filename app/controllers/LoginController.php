<?php
/**
 * Created by PhpStorm.
 * User: larry
 * Date: 10/7/2015
 * Time: 7:50 PM
 */

class LoginController extends BaseController{

    public function login() {

        $data = array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        );

        $rules = array(
            'username' => 'required|exists:tbl_user,user_username',
            'password' => 'required'
        );


        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {

            return Redirect::to('login')
                ->withErrors($validator)
                ->withInput(Input::except('password'));

        } else {

            $user = User::where('user_username', '=',$data['username'])
                ->where('user_password', '=',GlobalFunctionController::encryptPassword($data['password']))
                ->first();

            if($user) {

                Auth::login($user);

                if (Auth::user()->user_type == 1) {
//                    $this->loopPyramidEarning(Auth::user()->user_dealer_id);
                    return Redirect::to('user/home');
                }else {
                    return Redirect::to('dashboard');
                }

            } else {

                Auth::logout();
                Session::flush();
                $Status='Incorrect password';
                return Redirect::to('login')
                    ->with('Status',$Status);

            }

        }

    }

}