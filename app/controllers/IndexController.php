<?php

class IndexController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    public $theme;

    public function __construct() {
        $this->theme = Theme::uses('mpi')->layout('default');
    }

    protected function index() {
        return $this->theme->of('mpi.home')->render();
    }

    protected function about() {
        return $this->theme->of('mpi.about')->render();
    }

    protected function product() {
        return $this->theme->of('mpi.product')->render();
    }

    protected function marketing_plan() {
        return $this->theme->of('mpi.marketing-plan')->render();
    }

    protected function contact_us() {
        return $this->theme->of('mpi.contact-us')->render();
    }

    protected function login() {
        $theme = Theme::uses('index')->layout('default');
        return $theme->of('login',array('Status' => ''))->render();
    }


}
