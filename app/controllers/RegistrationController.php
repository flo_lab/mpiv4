<?php

class RegistrationController extends BaseController{
    public $theme;
    public $themeIndex;
    public function __construct() {
        $this->theme = Theme::uses('default')->layout('default');
        $this->themeIndex = Theme::uses('index')->layout('default');
    }

    public function index(){


        if($_POST){

            $genCode=Input::get('genCode');
            $placement=Input::get('placement');
            $sponsor=Input::get('sponsor');
            $tin=Input::get('tin');
            $position=intval(Input::get('position'));
            $lastname=Input::get('lastname');
            $firstname=Input::get('firstname');
            $middlename=Input::get('middlename');
            $address=Input::get('address');
            $email=Input::get('email');
            $contact=Input::get('contact');
            $tac = Input::get('iagree');
            $date = str_replace('/', '-', Input::get('birthdate'));
            $birthdate = date('Y-m-d', strtotime($date));

            $rules = array(
                'genCode'       => 'required',
                'placement'     => 'required',
                'sponsor'       => 'required',
                'position'      => 'required',
                'lastname'      => 'required',
                'firstname'     => 'required',
                'middlename'    => 'required',
                'iagree'        => 'required'
            );

            if($tac!=null){
                $agree = 1;
            }else{
                $agree = 0;
            }
            $lastDealerCode = $this->userModel()->lastUser();

            if ($lastDealerCode) {
                $count=$lastDealerCode->user_id+1;

                $dealerNum= sprintf("%05d",$count);
            } else {
                $dealerNum= sprintf("%05d",1);
            };

            /***Get Package Type By Inserted Control Number***/
            $packageType = $this->payInCodeModel()->selectPayInCodeByPayInCode($genCode);
            $package=$packageType->pay_in_package_type;
            $packageData = $this->packageModel()->selectPackageByPackageId($package);
            $earnAmount=$packageData->package_referral_earn;
            $earnPoints =$packageData->package_points;
            $data = array(
                'gencode'       => $genCode,
                'placement'     => $placement,
                'sponsor'       => $sponsor,
                'tin'           => $tin,
                'position'      => $position,
                'lastname'      => ucwords($lastname),
                'firstname'     => ucwords($firstname),
                'middlename'    => ucwords($middlename),
                'address'       => $address,
                'email'         => $email,
                'contact'       => $contact,
                'birthdate'     => $birthdate,
                'iagree'        => $agree,
                'username'      => Input::get('userName'),
                'usertype'      => 1,
                'payinstatus'   => 2,
                'dealer_no'     => $dealerNum,
                'password'      => GlobalFunctionController::encryptPassword(Input::get('userPassword')),
                'securityCode'  => Input::get('securityCode'),
                'packageType'   => $package

            );

            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()) {

                return Redirect::to('registration')
                    ->withErrors($validator)
                    ->withInput(Input::except('password'));

            }else{

                /****check if gen code is exist and active*****/

                $check = $this->payInCodeModel()->selectPayInCodeByCode($data);
                $placement = $this->userModel()->getUserByGenCode($data['placement']);
                $sponsor_data =$this->userModel()->getUserByGenCode($data['sponsor']);
                $placement_position =$this->userDownlineModel()->getUserDownlineByPlacement($data['placement']);
                $packageDa = $this->packageModel()->selectPackageByFirst();
//                $packageLowestPoints = $packageDa->package_points;
//                $packageLowestPairingEarn = $packageDa->package_pairing_earn;

                $checkUserOwnerCount = $this->userModel()->checkUserOwnerCount($data);
                if($checkUserOwnerCount == true) {

                    return Redirect::to('registration')
                        ->with('status','Number of accounts already exceeded.');

                } else {

                    return DB::transaction(function() use(
                        $position,
                        $sponsor,
                        $packageType,
                        $package,
                        $packageData,
                        $data,
                        $earnAmount,
                        $earnPoints,
                        $check,
                        $placement,
                        $sponsor_data,
                        $placement_position,
                        $packageDa){


                        if($check){

                            if($check->pay_in_status == 1) {
                                /*****check sponsor code*****/
                                if($sponsor_data){

                                    /*****check placement code*****/

                                    if($placement){

                                        /****check place position*****/
                                        if($placement_position==null){

                                            /*****get the placement binary position*****/

                                            $bp = $this->userModel()->selectUserByDealer($data['placement']);
                                            $sbp_result = $this->userModel()->selectUserByDealer($data['sponsor']);

                                            /******check sponsor binary position******/

                                            if ($data['sponsor'] == $data['placement']) {
                                                $dp = $data['position'];
                                                $sbp = $dp == LEFT ? "L" : "R";

                                            } else {
                                                $dp = $data['position'];
                                                $sponsorbp = $sbp_result->user_binary_position;
                                                $placementbp = $bp->user_binary_position;
                                                $n = strlen($placementbp) - strlen($sponsorbp);
                                                $sp = substr($placementbp, $n - 1, 1);
                                                $sbp = $sp;

                                            }
                                            /******for binary position******/
                                            if ($data['position'] == LEFT) {
                                                $binarypos = $bp->user_binary_position . 'L';
                                            } else {
                                                $binarypos = $bp->user_binary_position . 'R';
                                            }

                                            $id = $this->userModel()->saveUser($data, $binarypos, $sbp);
                                            $dealerId = $this->userModel()->selectUserById($id);

                                            $this->userDownlineModel()->saveUserDownline($data, $dealerId);
                                            $userDetailBySponsor = $this->userModel()->selectUserByDealer($sponsor);
                                            /*****direct referral earnings computation****/


                                            $dataArray=array(
                                                'userId'    => $userDetailBySponsor->user_id,
                                                'earnAmount'  => $earnAmount,
                                                'earnPoints'  => $earnPoints
                                            );

                                            $this->userEarningsModel()->saveEarning($dataArray);
                                            $currentEarnedUser = $userDetailBySponsor->user_total_earnings;
                                            $totalEarned = $currentEarnedUser + ($dataArray['earnAmount']);
                                            $dataArray['totalEarned']   = $totalEarned;
                                            $this->userModel()->updateUserEarning($dataArray);

                                            /***********update geonology tree points************/

                                            $arr = array(
                                                'new_dealer'                => $dealerId,
                                                'new_recruit_package'       => $package,
                                                'user_id'                   => $bp->user_id,
                                                'position'                  => $data['position'],
                                                'packageLowestPoints'       => $packageDa->package_points,
                                                'packageLowestPairingEarn'  => $packageDa->package_pairing_earn
                                            );
                                            $this->recursive($arr);

                                            /***update placement status***/
                                            $_status = $this->statusUpdate($data['placement']);
                                            $this->userModel()->updateUserStatus($data['placement'],$_status);
                                            $this->payInCodeModel()->updatePayInStatusByCode($data);

                                            return Redirect::to('registration/success');

                                        }else{


                                            $left=null;$right=null;
                                            foreach($placement_position as $val){
                                                if($val->user_rec_downline_position==0){
                                                    $left=$val->user_rec_downline_position;
                                                }else{
                                                    $right=$val->user_rec_downline_position;
                                                }
                                            }

                                            if($position==0){

                                                if($left==null){
                                                    /*****get the placement binary position*****/
                                                    $bp = $this->userModel()->selectUserByDealer($data['placement']);
                                                    $sbp_result = $this->userModel()->selectUserByDealer($data['sponsor']);

                                                    /******check sponsor binary position*****/
                                                    if ($data['sponsor'] == $data['placement']) {
                                                        $dp = $data['position'];
                                                        $sbp = $dp == LEFT ? "L" : "R";

                                                    } else {
                                                        $dp = $data['position'];
                                                        $sponsorbp = $sbp_result->user_binary_position;
                                                        $placementbp = $bp->user_binary_position;
                                                        $n = strlen($placementbp) - strlen($sponsorbp);
                                                        $sp = substr($placementbp, $n - 1, 1);
                                                        $sbp = $sp;

                                                    }
                                                    /*****for binary position******/
                                                    if ($data['position'] == LEFT) {
                                                        $binarypos = $bp->user_binary_position . 'L';
                                                    } else {
                                                        $binarypos = $bp->user_binary_position . 'R';
                                                    }

                                                    $id = $this->userModel()->saveUser($data, $binarypos, $sbp);
                                                    $dealerId = $this->userModel()->selectUserById($id);

                                                    $this->userDownlineModel()->saveUserDownline($data, $dealerId);

                                                    /*****direct referral earnings computation****/
                                                    $userDetailBySponsor = $this->userModel()->selectUserByDealer($sponsor);
                                                    $dataArray=array(
                                                        'userId'    => $userDetailBySponsor->user_id,
                                                        'earnAmount'  => $earnAmount,
                                                        'earnPoints'  => $earnPoints
                                                    );

                                                    $this->userEarningsModel()->saveEarning($dataArray);
                                                    $currentEarnedUser = $userDetailBySponsor->user_total_earnings;
                                                    $totalEarned = $currentEarnedUser + ($dataArray['earnAmount']);
                                                    $dataArray['totalEarned']   = $totalEarned;
                                                    $this->userModel()->updateUserEarning($dataArray);

                                                    /***********update geonology tree************/
                                                    $arr = array(
                                                        'new_dealer'                => $dealerId,
                                                        'new_recruit_package'       => $package,
                                                        'user_id'                   => $bp->user_id,
                                                        'position'                  => $data['position'],
                                                        'packageLowestPoints'       => $packageDa->package_points,
                                                        'packageLowestPairingEarn'  => $packageDa->package_pairing_earn
                                                    );
                                                    $this->recursive($arr);

                                                    /****update placement status ****/

                                                    $_status = $this->statusUpdate($data['placement']);
                                                    $this->userModel()->updateUserStatus($data['placement'],$_status);

                                                    $this->payInCodeModel()->updatePayInStatusByCode($data);


                                                    return Redirect::to('registration/success');


                                                }else{
                                                    return Redirect::to('registration')->with('status','Placement position (left) is occupied.');
                                                }

                                            }else{

                                                if($right==null){
                                                    /*****get the placement binary position******/
                                                    $bp = $this->userModel()->selectUserByDealer($data['placement']);
                                                    $sbp_result = $this->userModel()->selectUserByDealer($data['sponsor']);

                                                    /*****check sponsor binary position*****/
                                                    if ($data['sponsor'] == $data['placement']) {
                                                        $dp = $data['position'];
                                                        $sbp = $dp == LEFT ? "L" : "R";

                                                    } else {
                                                        $dp = $data['position'];
                                                        $sponsorbp = $sbp_result->user_binary_position;
                                                        $placementbp = $bp->user_binary_position;
                                                        $n = strlen($placementbp) - strlen($sponsorbp);
                                                        $sp = substr($placementbp, $n - 1, 1);
                                                        $sbp = $sp;

                                                    }
                                                    /*****for binary position*****/
                                                    if ($data['position'] == LEFT) {
                                                        $binarypos = $bp->user_binary_position . 'L';
                                                    } else {
                                                        $binarypos = $bp->user_binary_position . 'R';
                                                    }

                                                    $id = $this->userModel()->saveUser($data, $binarypos, $sbp);
                                                    $dealerId = $this->userModel()->selectUserById($id);

                                                    $this->userDownlineModel()->saveUserDownline($data, $dealerId);

                                                    /*****direct referral earnings computation****/
                                                    $userDetailBySponsor = $this->userModel()->selectUserByDealer($sponsor);
                                                    $dataArray=array(
                                                        'userId'    => $userDetailBySponsor->user_id,
                                                        'earnAmount'  => $earnAmount,
                                                        'earnPoints'  => $earnPoints
                                                    );


                                                    $this->userEarningsModel()->saveEarning($dataArray);
                                                    $currentEarnedUser = $userDetailBySponsor->user_total_earnings;
                                                    $totalEarned = $currentEarnedUser + ($dataArray['earnAmount']);
                                                    $dataArray['totalEarned']   = $totalEarned;
                                                    $this->userModel()->updateUserEarning($dataArray);

                                                    /***********update geonology tree************/
                                                    $arr = array(
                                                        'new_dealer'                => $dealerId,
                                                        'new_recruit_package'       => $package,
                                                        'user_id'                   => $bp->user_id,
                                                        'position'                  => $data['position'],
                                                        'packageLowestPoints'       => $packageDa->package_points,
                                                        'packageLowestPairingEarn'  => $packageDa->package_pairing_earn
                                                    );
                                                    $this->recursive($arr);

                                                    /****update placement status ****/
                                                    $_status = $this->statusUpdate($data['placement']);
                                                    $this->userModel()->updateUserStatus($data['placement'],$_status);

                                                    $this->payInCodeModel()->updatePayInStatusByCode($data);

                                                    return Redirect::to('registration/success');


                                                }else{
                                                    return Redirect::to('registration')->with('status','Placement position (right) is occupied.');
                                                }
                                            }


                                        }

                                    }else{
                                        return Redirect::to('registration')->with('status','Placement is not existed.');
                                    }

                                }else{
                                    return Redirect::to('registration')->with('status','Sponsor is not existed.');
                                }


                            } else {

                                return Redirect::to('registration')
                                    ->with('status','Control number is no longer available.');
                            }

                        }else{
                            return Redirect::to('registration')
                                ->with('status','Code does not exist.');
                        }



                    });


                }



            }

            /***
             * Get registration view
             *
             */
        }else{
            return $this->themeIndex->of('registration'/*,array('Status' => '')*/)->render();
        }

    }


    public function ajaxValidateRemote()
    {
        $type = Input::get('type');
        switch ($type) {

            case 'check-generated-code': {

                if ($this->payInCodeModel()->checkGenerateCode(Input::get('generatedCode')))
                    echo 'true';
                else
                    echo 'false';
                break;

            }
            case 'check-security-code': {

                if ($this->payInCodeModel()->checkSecurityCodeAndGeneratedCode(Input::get('generatedSecurityCode')))
                    echo 'true';
                else
                    echo 'false';
                break;

            }
            case 'check-given-codes': {

                $paramArray = array(
                    'generatedCode' => Input::get('genCode'),
                    'securityCode' => Input::get('generatedSecurityCode'),
                );

                if ($this->payInCodeModel()->checkSecurityCodeAndGeneratedCode($paramArray))

                    echo 'true';
                else
                    echo 'false';

                break;

            }
            case 'check-user-dealer': {

                $placementUser = Input::get('placementUser');
                $sponsorUser = Input::get('sponsorUser');

                if ($placementUser != null)
                    $value = $placementUser;
                else
                    $value = $sponsorUser;


                if ($this->registrationModel()->checkPlacementUser($value))
                    echo 'true';
                else
                    echo 'false';
                break;

            }
            case 'check-user-name': {

                if ($this->registrationModel()->checkUserAvailable(Input::get('userName')))
                    if(Input::get('userLogin') != null)
                        echo 'false';
                    else
                        echo 'true';
                else
                    if(Input::get('userLogin') != null)
                        echo 'true';
                    else
                        echo 'false';

                break;

            }
            case 'check-user-email': {

                if ($this->userModel()->selectDealerByEmail(Input::get('email')))
                    echo 'false';
                else
                    echo 'true';
                break;
            }
            case 'check-placement': {

                $left = null;
                $right = null;

                //$placement_position = $this->userDownlineModel()->getUserDownlineByPlacement(Input::get('placementCheck'));
                $data=array(
                    'placement' =>Input::get('placementCheck'),
                    'placement_position' =>Input::get('position'),
                );
                $placement_position = $this->userModel()->selectUserDownlineByPlacement($data);
//                foreach ($placement_position as $val) {
//
//                    if ($val->user_placement_position == 0) {
//                        $left = $val->user_placement_position;
//                    } else {
//                        $right = $val->user_placement_position;
//                    }
//
//                }

                //$pos = [$left, $right];

                if ($placement_position===null) {

                    return 'true';

                } else {
                    return 'false';

                }

                break;

            }
        }
    }

    public function success(){
        return $this->theme->of('registration_success')->render();
    }
    public function userDashboard(){
        return $this->theme->of('user.dashboard'/*,array('Status' => '')*/)->render();
    }
}