<?php
Class AjaxGlobalController extends BaseController{

    public function ajax()
    {
        $type = Input::get('type');
//        $return = array();
//        $transaction='';

        switch($type) {
            case 'earning-list':{

                $transaction = DB::table('tbl_user_earning')
                    ->where('user_earn_user', '=', Auth::user()->user_id)->get();
                $dataArr=array();
                foreach ($transaction as $aRow){

                    $data = array(
                        $aRow->user_earn_amount,
                        $aRow->user_earn_points,
                        $aRow->user_earn_date

                    );
                    array_push($dataArr,$data);
                }

                return Response::json(
                    array(
                        'result' => $dataArr
                    )
                );
                break;
            }
            case 'withdraw':{
                $withDrawtype = Input::get('withdrawType');
                $accountName = Input::get('accountName');
                $accountNo = Input::get('accountNo');
                $data = array(
                    'dealer' => Auth::user()->user_id,
                    'withdraw_type' => $withDrawtype,
                    'withdraw_account' => $accountNo,
                    'withdraw_account_name' => $accountName,
                    'withdraw_amount' => Auth::user()->user_total_earnings,
                    'withdraw_date' => new DateTime()
                );

                try{
                    $success= DB::transaction(function() use($data){
                        $this->userWithdrawModel()->saveUserWithdraw($data);
                        return true;
                    });

                }catch(Exception $ex){
                    $success = false;
                }

                return Response::json(

                    array(
                        'result' => $success
                    )
                );
                break;
            }
            case 'downline-list':{

                $transaction=$this->userEarnPointsModel()->selectEarnPointsByDealerLeftJoinUserTable(Auth::User()->user_id);

                $dtResult = $this->setDatatable($transaction, array(
                    'downline_dealer',
                    'user_lname',
                    'user_fname',
                    'user_mname',
                    'user_package_type',
                    'left_points',
                    'right_points',
                    'user_date_reg'
                ),
                    'id');

                foreach ($dtResult['objResult'] as $aRow){
                    if($aRow->left_points!=0) {
                        $position = 'Left';
                    }else{
                        $position = 'Right';
                    }
                    $data = array(
                        $aRow->downline_dealer,
                        $aRow->user_lname,
                        $aRow->user_fname,
                        $aRow->user_mname,
                        $aRow->user_package_type,
                        $position,
                        $aRow->user_date_reg

                    );
                    $dtResult['aaData'][] = $data;
                }

                unset($dtResult['objResult']);
                echo json_encode($dtResult);
                break;
            }
            case 'withdraw-list':{

                $transaction=DB::table('tbl_user_withdraw')
                    ->where('user_withdraw_user',Auth::User()->user_id);

                $dtResult = $this->setDatatable($transaction, array(
                    'user_withdraw_id',
                    'user_withdraw_user',
                    'user_withdraw_type',
                    'user_withdraw_amount',
                    'user_withdraw_date',
                    'user_withdraw_recieve_date',
                    'user_withdraw_status'

                ),
                    'user_withdraw_id');

                foreach ($dtResult['objResult'] as $aRow){
                    if($aRow->user_withdraw_type == CASH){
                        $withdraw_type="CASH";
                    }else if($aRow->user_withdraw_type == BANK){
                        $withdraw_type="BANK";
                    }else{
                        $withdraw_type="CHECK";
                    }
                    if( $aRow->user_withdraw_recieve_date!=null){
                        $date_receive=$aRow->user_withdraw_recieve_date;
                    }else{
                        $date_receive="Not yet receive";
                    }
                    $data = array(
                        $aRow->user_withdraw_amount,
                        $aRow->user_withdraw_date,
                        $date_receive,
                        $withdraw_type

                    );
                    $dtResult['aaData'][] = $data;
                }

                unset($dtResult['objResult']);
                echo json_encode($dtResult);
                break;
            }
        }
    }
}

?>