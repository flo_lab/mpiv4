<?php

class UserEarningsController extends BaseController{

    public $theme;
    public function __construct() {
        $this->theme = Theme::uses('mpi')->layout('user-dashboard');
    }

    public function earnings(){
        return $this->theme->of('user.earnings')->render();
    }
}
?>