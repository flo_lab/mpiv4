<?php

class UserController extends BaseController{
    public $theme;
    public function __construct() {
        $this->theme = Theme::uses('mpi')->layout('user-dashboard');
    }

    public function logout(){

        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }

    public function profile(){
        $data=array(
            'user' => DB::table('tbl_user')
                    ->where('user_username',Auth::user()->user_username)
                    ->first()
        );

        return $this->theme->of('user.profile',$data)->render();
    }

    public function home(){

        return $this->theme->of('user.home'/*,$data*/)->render();
    }

    public function cashout(){


        try{
            $withdraw = $this->userWithdrawModel()->selectUserWithdrawByUser(0);
            $withdrawStatus = $withdraw->user_withdraw_status;
            $withdrawAmount = $withdraw->user_withdraw_amount;
        }catch(Exception $ex){

            $withdraw2 = $this->userWithdrawModel()->selectUserWithdrawByUser(1);
            if($withdraw2===null){
                $withdrawStatus=null;
                $withdrawAmount = 0;
            }else{
                $withdrawStatus = 1;
                $withdrawAmount = $withdraw2->user_withdraw_amount;
            }

        }
        $leftRecruit = count($this->userModel()->getUserByUserSponsorPositionLeft(Auth::user()->user_dealer_id));
        $rightRecruit = count($this->userModel()->getUserByUserSponsorPositionRight(Auth::user()->user_dealer_id));
        $sql = $this->userWithdrawModel()->getWithdrawHistoryByUser();
        $totalWithdraw=0;
        foreach($sql as $row){
            if($row->user_withdraw_status==PAID){
                $totalWithdraw=$totalWithdraw+$row->user_withdraw_amount;
            }
        }
        $data = array(
            'status' => $withdrawStatus,
            'left' => $leftRecruit,
            'right' => $rightRecruit,
            'available_balance' => Auth::User()->user_total_earnings - $withdrawAmount,
            'total_withdraw' => $totalWithdraw
        );

        return $this->theme->of('user.cashout',$data)->render();
    }

    public function arrayFunction($downlineFirst){
        $firstLevelInitital=[];
        if(count($downlineFirst)>1){

            $arrIndexZero=array(

                'user_rec_downline'=>$downlineFirst[0]->user_rec_downline,
                'user_rec_downline_position'=>$downlineFirst[0]->user_rec_downline_position,
            );
            $arrIndexOne=array(

                'user_rec_downline'=>$downlineFirst[1]->user_rec_downline,
                'user_rec_downline_position'=>$downlineFirst[1]->user_rec_downline_position,
            );
            if($downlineFirst[1]->user_rec_downline_position==LEFT){
                array_push($firstLevelInitital,$arrIndexOne);
                array_push($firstLevelInitital,$arrIndexZero);
            }else{
                array_push($firstLevelInitital,$arrIndexZero);
                array_push($firstLevelInitital,$arrIndexOne);
            }

        }else if(count($downlineFirst)==1){

            $arrLeftRight=array(
                'user_rec_downline'=>$downlineFirst[0]->user_rec_downline,
                'user_rec_downline_position'=>$downlineFirst[0]->user_rec_downline_position,
            );
            $arrNull=array(

                'user_rec_downline'=>null,
                'user_rec_downline_position'=>null,
            );
            if($downlineFirst[0]->user_rec_downline_position==RIGHT){
                array_push($firstLevelInitital,$arrNull);
                array_push($firstLevelInitital,$arrLeftRight);
            }else{
                array_push($firstLevelInitital,$arrLeftRight);
                array_push($firstLevelInitital,$arrNull);
            }

        }else{
            $arr=array(

                'user_rec_downline'=>null,
                'user_rec_downline_position'=>null,
            );
            array_push($firstLevelInitital,$arr);
            array_push($firstLevelInitital,$arr);
        }

        return $firstLevelInitital;
    }

    public function geneology(){
        $firstLevel=[];
        $secondLevel=[];

        $dealer=Input::get('dealerId');
//        $dealerStatus=Input::get('dealerStatus');

        if($dealer!=''){
            $head = $dealer;
            $head_name=DB::table('tbl_user')->where('user_dealer_id',$head)->first();
            $status=$this->statusUpdate($head);
            $downlineFirst= $this->userDownlineModel()->getUserDownlineByPlacement($dealer);
        }else{
            $head =Auth::User()->user_dealer_id;
            $head_name=DB::table('tbl_user')->where('user_dealer_id',$head)->first();
            $status=$this->statusUpdate($head);
            $downlineFirst= $this->userDownlineModel()->getUserDownlineByPlacement($head);
        }

        $firstLevelInitital=$this->arrayFunction($downlineFirst);
        /****for 1st Level*****/
        for ($i = 0; $i <= 1; $i++) {
                if (count($firstLevelInitital) > 0) {

                    try {
                        $user = $this->userModel()->selectUserByDealer($firstLevelInitital[$i]['user_rec_downline']);
                        $arr = array(
                            'status' => $this->statusUpdate($firstLevelInitital[$i]['user_rec_downline']),
                            'name' => $user->user_lname . ' ' . $user->user_fname . ' ' . $user->user_mname,
                            'name_tag' => $user->user_lname . ' ' . $user->user_fname,
                            'username' => $user->user_username,
                            'date_reg' => $user->user_date_reg,
                            'downline' => $firstLevelInitital[$i]['user_rec_downline'],
                            'position' => $firstLevelInitital[$i]['user_rec_downline_position'],
                        );

                        array_push($firstLevel, $arr);
                    } catch (Exception $ex) {
                        $arr = array(
                            'status' => null,
                            'name' => null,
                            'name_tag' => null,
                            'username' => null,
                            'date_reg' => null,
                            'downline' => null,
                            'position' => null,
                        );
                        array_push($firstLevel, $arr);
                    }
                } else {
                    $arr = array(
                        'status' => null,
                        'name' => null,
                        'name_tag' => null,
                        'username' => null,
                        'date_reg' => null,
                        'downline' => null,
                        'position' => null,
                    );
                    array_push($firstLevel, $arr);
                }
            }

        /****for 2nd level****/
        foreach($firstLevel as $val){
            $upline = $val['downline'];
            $downline= $this->userDownlineModel()->getUserDownlineByPlacement($upline);
            $firstLevelInitital=$this->arrayFunction($downline);
            for($x=0;$x<=1;$x++){
                if(count($firstLevelInitital)>0){
                   try{
                        $user = $this->userModel()->selectUserByDealer($firstLevelInitital[$x]['user_rec_downline']);
                        $arr1=array(
                            'status'=>$this->statusUpdate($firstLevelInitital[$x]['user_rec_downline']),
                            'name'=>$user->user_lname.' '.$user->user_fname.' '.$user->user_mname,
                            'name_tag'=>$user->user_lname.' '.$user->user_fname,
                            'username' => $user->user_username,
                            'date_reg'=>$user->user_date_reg,
                            'upline' => $upline,
                            'downline'=>$firstLevelInitital[$x]['user_rec_downline'],
                            'position'=>$firstLevelInitital[$x]['user_rec_downline_position'],
                            'head_position'=>$val['position']
                        );
                        array_push($secondLevel,$arr1);
                    }catch(Exception $ex){
                        $arr1=array(
                            'status' => null,
                            'name' =>null,
                            'name_tag' =>null,
                            'username' => null,
                            'date_reg' =>null,
                            'upline' => null,
                            'downline'=>null,
                            'position'=>null,
                            'head_position'=>$val['position']
                        );
                        array_push($secondLevel,$arr1);
                    }
                }else{
                    $arr1=array(
                        'status' => null,
                        'name' =>null,
                        'name_tag' =>null,
                        'username' => null,
                        'date_reg' =>null,
                        'upline' => null,
                        'downline'=>null,
                        'position'=>null,
                        'head_position'=>$val['position']
                    );
                    array_push($secondLevel,$arr1);
                }
            }
        }

        /***for left and right downline****/
        $res=$this->userEarnPointsModel()->selectEarnPointsByDealer(Auth::User()->user_id);

        $downlineLeft=0;
        $downlineRight=0;
        foreach($res as $val){
            if($val->left_points!=0){
                $downlineLeft++;
            }else{
                $downlineRight++;
            }
        }



        $data= array(
            'head'  => $head,
            'head_name' => $head_name,
            'headStatus'  => $status,
            'first' => $firstLevel,
            'second' => $secondLevel,
            'left_downline' =>$downlineLeft,
            'right_downline' =>$downlineRight
        );

        return $this->theme->of('user.geneology',$data)->render();
    }
}