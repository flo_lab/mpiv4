<?php

return array(
    'validation_messages' => array(
        'required' => 'Required',
        'email' => 'Invalid email',
        'min' => ':min minimum characters',
        'check_username' => 'Username already exist',
        'check_email' => 'email already exist',
        'check_password' => 'Password does not match'
    ),
);
