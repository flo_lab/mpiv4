<?php
/**
 * Created by PhpStorm.
 * User: Arjay
 * Date: 3/28/2016
 * Time: 11:14 AM
 */

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserPairingPoints extends Eloquent implements UserInterface, RemindableInterface
{

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_user_pairing_points';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    protected $primaryKey = 'id';

    public function selectByDealerLimitOne($data){
        $result = DB::table($this->table)
            ->where('dealer_id',$data)
            ->orderBy('id', 'desc')
            ->first();
        return $result;
    }

    public function selectByDealerAndCurrentDate($data){
        $date=new DateTime();
        $current=$date->format('Y-m-d');
        $result = DB::table($this->table)
            ->where('dealer_id',$data)
            ->whereBetween('date_paired', array($current, $current))
            ->get();
        return $result;
    }

    public function savePairingPoints($data){

        try{
            if($data['placement_position']==intval(LEFT)){
                $res=DB::table($this->table)->insertGetId(
                    array(
                        'dealer_id'    => $data['dealer_id'],
                        'left_points'  => $data['points'],
                        'right_points'  => 0,
                        'date_input'    => new DateTime()
                    )
                );
            }else if($data['placement_position']==intval(RIGHT)){
                $res=DB::table($this->table)->insertGetId(
                    array(
                        'dealer_id'    => $data['dealer_id'],
                        'left_points'  => 0,
                        'right_points'  => $data['points'],
                        'date_input'    => new DateTime()
                    )
                );
            }
        }catch (Exception $ex){
            $res=false;

        }
        return $res;
    }

    public function updatePairingLeftPoints($data){
        DB::table($this->table)
            ->where('id',$data['id'])
            ->update(
                array(
                    'left_points'=>$data['points'],
                    'date_paired'=>new DateTime()
                )
            );
    }
    public function updatePairingRightPoints($data){
        DB::table($this->table)
            ->where('id',$data['id'])
            ->update(
                array(
                    'right_points'=>$data['points'],
                    'date_paired'=>new DateTime()
                )
            );
    }

    public function updatePairingPoints($data,$earnedPoints){
        if($data['placement_position']==LEFT){
            DB::table($this->table)
                ->where('id',$data['id'])
                ->update(
                    array(
                        'left_points'=>$data['points']+$earnedPoints
                    )
                );
        }else{
            DB::table($this->table)
                ->where('id',$data['id'])
                ->update(
                    array(
                        'right_points'=>$data['points']+$earnedPoints
                    )
                );
        }

    }

}