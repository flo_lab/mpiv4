<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserWithdraw extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_user_withdraw';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    protected $primaryKey = 'user_withdraw_id';



    public function saveUserWithdraw($data) {

        $res=DB::table($this->table)->insertGetId(
            array(
                'user_withdraw_user'    => $data['dealer'],
                'user_withdraw_type'  => $data['withdraw_type'],
                'user_withdraw_account'  => $data['withdraw_account'],
                'user_withdraw_account_name'    => $data['withdraw_account_name'],
                'user_withdraw_amount'    => $data['withdraw_amount'],
                'user_withdraw_status'    => 0,
                'user_withdraw_date'    => new DateTime(),
            )
        );
        return $res;

    }

    public function selectUserWithdrawByUser($data){
        $result=DB::table($this->table)
            ->where('user_withdraw_user',Auth::User()->user_id)
            ->where('user_withdraw_status',$data)
            ->orderBy('user_withdraw_id', 'desc')
            ->first();
        return $result;
    }

    public function getPayoutList(){
        $result=DB::table($this->table)->where('user_withdraw_status',0)
            ->join('tbl_user', 'tbl_user_withdraw.user_withdraw_user', '=', 'tbl_user.user_id');
        return $result;
    }

    public function updateStatus($id){
        DB::table($this->table)
            ->where('user_withdraw_id',$id)->
            update(
                array(
                    'user_withdraw_status'=>PAID,
                    'user_withdraw_recieve_date'=>new DateTime(),
                    'user_withdraw_assisted'=>Auth::User()->user_id
                )
            );
    }

    public function getAmount($id){
        return DB::table($this->table)->where('user_withdraw_id',$id)->first();
    }

    public function getWithdrawHistoryByUser(){
        $result=DB::table($this->table)
            ->where('user_withdraw_user',Auth::User()->user_id)
            ->get();
        return $result;
    }
}
?>