<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tbl_user';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    protected $primaryKey = 'user_id';

    public function  saveUser($data,$bp,$sbp)
    {

        $lvl = strlen($bp);
        $res=DB::table($this->table)->insertGetId(
            array(
                'user_dealer_id'    => $data['dealer_no'],
                'user_fname'        => $data['firstname'],
                'user_lname'        => $data['lastname'],
                'user_mname'        => $data['middlename'],
                'user_email'        => $data['email'],
                'user_contact_no'   => $data['contact'],
                'user_birthdate'    => $data['birthdate'],
                'user_tin_no'       => $data['tin'],
                'user_terms_agree'  => $data['iagree'],
                'user_reg_code'     => $data['gencode'],
                'user_package_type' => $data['packageType'],
                'user_sponsor'      => $data['sponsor'],
                'user_placement'    => $data['placement'],
                'user_placement_position'  => $data['position'],
                'user_binary_position'  => $bp,
                'user_level'  => $lvl,
                'user_sponsor_position'  => $sbp,
                'user_username'     => $data['username'],
                'user_password'     => $data['password'],
                'user_type'         => $data['usertype'],
                'user_date_reg'         => new DateTime()
            )
        );

        return $res;
    }
    public function  updatePasswordById($data){
        $result=DB::table($this->table)
            ->where('user_id',$data['id'])
            ->update(
                array('user_password'=>GlobalFunctionController::encryptPassword($data['new-password'])
                )
            );
        return $result;
    }
    public function  removePasswordTokenById($data){
        $result=DB::table($this->table)
            ->where('user_id',$data['id'])
            ->update(
                array('user_change_password_token'=>null
                )
            );
        return $result;
    }

    public function  saveChangePasswordTokenById($data){
        $result=DB::table($this->table)
            ->where('user_id',$data['id'])
            ->update(
                array(
                    'user_change_password_token'=>$data['encryptedParam'],
                    'user_change_password_token_date'=>new DateTime()
                )

            );
        return $result;
    }

    public function getUserByGenCode($data){
        return DB::table($this->table)
            ->where('user_dealer_id',$data)
            ->first();
    }
    public function selectUserById($data){
        return DB::table($this->table)->where('user_id',$data)->first()->user_dealer_id;
    }
    public function lastUser()
    {
        $id=DB::table($this->table)->orderBy('user_id', 'DESC')->first();

        return $id;
    }
    public function selectUserByDealer($data){
        $id = DB::table($this->table)->where('user_dealer_id',$data)->first();
        return $id;
    }
    public function selectUserByUserId($data){
        $id = DB::table($this->table)->where('user_id',$data)->first();
        return $id;
    }
    public function updateUserStatus($dealer,$stat){
        try{
            DB::table($this->table)
                ->where('user_dealer_id', $dealer)
                ->update(
                    array(
                        'user_status'  => $stat
                    )
                );
            $res=true;
        }catch(Exception $ex){
            $res=false;
        }
        return $res;

    }
    public function checkUserOwnerCount($data){

        $count = DB::table($this->table)
            ->where('user_fname',$data['firstname'])
            ->where('user_fname',$data['middlename'])
            ->where('user_lname',$data['lastname'])
            ->get();

        if(count($count) == MAX_NUMBER_ACCOUNTS)
            return true;
         else
             return false;
      }
    public function updateUserEarning($data) {

        DB::table($this->table)
            ->where('user_id', $data['userId'])
            ->update(array(
                    'user_total_earnings'  => $data['totalEarned']
                )
            );
    }
    public function getUserByUserSponsorPositionLeft($data){
       $result= DB::table($this->table)
            ->where('user_sponsor_position','L')
            ->where('user_sponsor',$data)
            ->get();
        return $result;
    }
    public function getUserByUserSponsorPositionRight($data){
        $result= DB::table($this->table)
            ->where('user_sponsor_position','R')
            ->where('user_sponsor',$data)
            ->get();
        return $result;
    }
    public function getDealerID($user_withdraw_user){
        return DB::table($this->table)->where('user_id',$user_withdraw_user)->first();
    }
    public function getDealerName($user_withdraw_user){
        return DB::table($this->table)->where('user_id',$user_withdraw_user)->first();
    }
    public function getTotalEarnings($id){
        return DB::table($this->table)->where('user_id',$id)->first();
    }
    public function updateTotalEarnings($id,$amount){
        return DB::table($this->table)->where('user_id',$id)->update(array('user_total_earnings'=>$amount));
    }
    public function selectUserDownlineByPlacement($data){
        return DB::table($this->table)
            ->where('user_placement',$data['placement'])
            ->where('user_placement_position',$data['placement_position'])
            ->first();
    }
    public function updateUserTotalPoints($data){
        if($data['placement_position']==LEFT){
            DB::table($this->table)
                ->where('user_id',$data['dealer_id'])
                ->update(
                    array(
                        'user_total_points_left'=>$data['points'] + $data['current_left_points'],
                    )
                );
        }else if($data['placement_position']==RIGHT){
            DB::table($this->table)
                ->where('user_id',$data['dealer_id'])
                ->update(
                    array(
                        'user_total_points_right'=>$data['points'] + $data['current_right_points'],
                    )
                );
        }

    }
    public function selectDealerByEmail($data){
        return DB::table($this->table)->where('user_email',$data)->first();
    }

}
