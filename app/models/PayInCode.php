<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PayInCode extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_pay_in_code';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    protected $primaryKey = 'pay_in_id';

    public $timestamps  = false;

    public function  savePayIncode($data)
    {

        $res=DB::table($this->table)->insertGetId(
            array(
                'pay_in_code'          => $data['code'],
                'pay_in_date'          => $data['date'],
                'pay_in_user'          => $data['user'],
                'pay_in_status'        => $data['status'],
                'pay_in_security_code' => $data['security'],
                'pay_in_package_type' => $data['package']

            )
        );

        return $res;
    }

    public function selectPayInCodeByPayInCode($data){
        return DB::table($this->table)
            ->where('pay_in_code',$data)
            ->first();
    }
    public function selectPayInCodeByCode($data){
        return DB::table($this->table)
            ->where('pay_in_code',$data['gencode'])
            ->where('pay_in_security_code',$data['securityCode'])
            ->first();
    }




    public function lastPayInCode()
    {
        $Code=DB::table($this->table)->orderBy('pay_in_id', 'DESC')->first();

        return $Code;
    }
    public function selectCodePerId($data){
        $Code=DB::table($this->table)
            ->leftjoin('tbl_user', 'user_id', '=', 'pay_in_user')
            ->where('pay_in_id',$data)
            ->first();
        return $Code;
    }

    public function updatePayInStatusByCode($data){
        DB::table($this->table)
            ->where('pay_in_code', $data['gencode'])
            ->update(
                array(
                    'pay_in_status'  => $data['payinstatus']

                )
            );
    }

    public function checkGenerateCode($param) {

        $checkGeneratedValid = DB::table($this->table)
            ->where('pay_in_code', $param)
            ->where('pay_in_status', '=', NEW_CODE)
            ->first();

        if($checkGeneratedValid)
            return true;
        else
            return false;

    }

    public function checkSecurityCodeAndGeneratedCode($dataArray) {

        $checkValidCodes = DB::table($this->table)
            ->where('pay_in_code',$dataArray['generatedCode'])
            ->where('pay_in_security_code',$dataArray['securityCode'])
            ->where('pay_in_status', '=', NEW_CODE)
            ->first();

        if($checkValidCodes)
            return true;
        else
            return false;

    }

    public function checkSecurityCodedCode($param) {

        $checkValidCodes = DB::table($this->table)
            ->where('pay_in_security_code',$param)
            ->first();

        if($checkValidCodes)
            return true;
        else
            return false;

    }

    public function lastCodeGeneratedByCount($data)
    {
        $id=DB::table($this->table)->orderBy('pay_in_id', 'DESC')->take($data)->get();

        return $id;
    }

}