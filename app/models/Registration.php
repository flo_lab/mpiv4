<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Registration extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_user';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    protected $primaryKey = 'user_id';

    public function checkPlacementUser($param) {

        $checkPlacement = DB::table($this->table)
            ->where('user_dealer_id', $param)
            ->first();

        if($checkPlacement)
            return true;
        else
            return false;

    }

    public function checkUserAvailable($param) {

        $checkUserAvailable = DB::table($this->table)
            ->where('user_username', $param)
            ->first();

        if($checkUserAvailable)
            return false;
        else
            return true;

    }



}
