<?php
/**
 * Created by PhpStorm.
 * User: Richard_17
 * Date: 3/12/2016
 * Time: 2:38 PM
 */

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserEarnPoints extends Eloquent implements UserInterface, RemindableInterface
{

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_user_earn_points';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    protected $primaryKey = 'id';

    public function saveEarnPoints($data){

        try{
            if($data['placement_position']==intval(LEFT)){
                $res=DB::table($this->table)->insertGetId(
                    array(

                        'dealer_id'    => $data['dealer_id'],
                        'left_points'  => $data['points'],
                        'right_points'  => 0,
                        'earn_points_date'    => new DateTime(),
                        'downline_dealer' => $data['new_dealer']
                    )
                );
            }else if($data['placement_position']==intval(RIGHT)){
                $res=DB::table($this->table)->insertGetId(
                    array(
                        'dealer_id'    => $data['dealer_id'],
                        'left_points'  => 0,
                        'right_points'  => $data['points'],
                        'earn_points_date'    => new DateTime(),
                        'downline_dealer' => $data['new_dealer']
                )
                );
            }
        }catch (Exception $ex){
            $res=false;
        }

        return $res;
    }

    public function selectEarnPointsByDealerAndByDate($data){

        $query = DB::table($this->table)
            ->where('dealer_id',$data['dealer'])
//            ->whereBetween('earn_points_date', array($data['date'], $data['date']))
            ->get();

        return $query;
    }

    public function selectEarnPointsByDealer($data){
        $query = DB::table($this->table)
            ->where('dealer_id',$data)
            ->get();
        return $query;
    }

    public function selectEarnPointsByDealerLeftJoinUserTable($data){
        $query = DB::table($this->table)
            ->join('tbl_user','user_dealer_id', '=', 'downline_dealer')
            ->join('tbl_package','tbl_package.package_id', '=', 'tbl_user.user_package_type')
            ->where('dealer_id',$data);

        return $query;

    }

}

