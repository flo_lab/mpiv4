<?php
/**
 * Created by PhpStorm.
 * User: Arjay Dacanay
 * Date: 3/26/2016
 * Time: 11:50 PM
 */

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Package extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_package';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    protected $primaryKey = 'package_id';

    public function selectPackageByPackageId($data){
        return DB::table($this->table)->where('package_id',$data)->first();
    }
    public function selectPackageByFirst(){
        return DB::table($this->table)->first();
    }

}