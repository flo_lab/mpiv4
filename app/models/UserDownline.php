<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserDownline extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_user_recruit';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    protected $primaryKey = 'user_rec_id';

    public function  saveUserDownline($data,$dealer)
    {

        $res=DB::table($this->table)->insertGetId(
            array(
                'user_rec_spon_id'              => $data['sponsor'],
                'user_rec_placement_id'         => $data['placement'],
                'user_rec_downline'             => $dealer,
                'user_rec_downline_position'    => $data['position']
            )
        );

        return $res;
    }

    public function getUserDownlineByPlacement($data){
        return DB::table($this->table)
            ->leftjoin('tbl_user','user_dealer_id', '=', 'user_rec_placement_id')
            ->where('user_rec_placement_id',$data)
            ->get();
    }

    public function selectUserDownlineByPlacement($data){
        return DB::table($this->table)
            ->leftjoin('tbl_user','user_dealer_id', '=', 'user_rec_placement_id')
            ->where('user_rec_placement_id',$data)->first();
    }

}
