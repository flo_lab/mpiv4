<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserEarnings extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_user_earning';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    protected $primaryKey = 'user_earn_id';



    public function saveEarning($data) {

        if($data['earnAmount']<0){
            $earnAmount = $data['earnAmount']*-1;
        }else{
            $earnAmount=$data['earnAmount'];
        }
        $res=DB::table($this->table)->insertGetId(
            array(
                'user_earn_user'    => $data['userId'],
                'user_earn_amount'  => $earnAmount,
//                'user_earn_points'  => $data['earnPoints'],
                'user_earn_date'    => new DateTime(),
               )
        );

        return $res;

    }

    public function selectEarningsByUserAmountPoints($data){
    $result=DB::table($this->table)
        ->where('user_earn_user',$data['userId'])
       // ->where('user_earn_amount',$data['earnAmount'])
        ->where('user_earn_points',$data['earnPoints'])
        ->first();
        return $result;
    }

    public function selectEarningsByUser($data){
        $result=DB::table($this->table)
            ->where('user_earn_user',$data)
            // ->where('user_earn_amount',$data['earnAmount'])
            //->where('user_earn_points',$data['earnPoints'])
            ->first();
        return $result;
    }



}
?>