<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before' => 'auth.login'), function()
{
    Route::match(array('GET','POST'),'user/earnings','UserEarningsController@earnings');
    Route::match(array('GET','POST'),'user/ajax','AjaxGlobalController@ajax');
    Route::match(array('GET','POST'),'user/geneology','UserController@geneology');
    Route::match(array('GET','POST'),'user/home','UserController@home');
    Route::match(array('GET','POST'),'user/profile','UserController@profile');
//    Route::match(array('GET','POST'),'user/home/entrep','UserController@entrep');
//    Route::match(array('GET','POST'),'user/home/bronze','UserController@bronze');
//    Route::match(array('GET','POST'),'user/home/silver','UserController@silver');
//    Route::match(array('GET','POST'),'user/home/gold','UserController@gold');
//    Route::match(array('GET','POST'),'user/home/diamond','UserController@diamond');

//    Route::match(array('GET','POST'),'user/profile','UserController@profile');
    Route::match(array('GET','POST'),'user/cashout','UserController@cashout');

    // admin routes
    Route::match(array('GET', 'POST'), 'dashboard', 'AdminPanelController@admin');
    Route::match(array('GET', 'POST'), 'admin/payin', 'AdminPanelController@payin');
    Route::match(array('GET', 'POST'), 'admin/ajax/printcode', 'AdminAjaxController@ajax');
    Route::match(array('GET', 'POST'), 'admin/searchtcode', 'GenerateCodeController@generateCode');
    Route::match(array('GET', 'POST'), 'admin/printTableCode', 'GenerateCodeController@printTableCode');
    Route::match(array('GET', 'POST'), 'admin/ajax', 'AdminAjaxController@ajax');
    Route::match(array('GET', 'POST'), 'admin/admin-cashout', 'AdminPanelController@adminCashout');


    Route::match(array('GET', 'POST'), 'admin/totalmember', 'AdminPanelController@member');
    Route::match(array('GET', 'POST'), 'admin/payout', 'AdminPanelController@payout');
    Route::match(array('GET', 'POST'), 'admin/transaction', 'AdminPanelController@transaction');
    Route::match(array('GET', 'POST'), 'admin/translog', 'AdminPanelController@transactionlog');
    Route::match(array('GET', 'POST'), 'admin/logout', 'AdminPanelController@logout');
    Route::match(array('GET', 'POST'), 'user/logout', 'UserController@logout');
});
Route::group(array('before' => 'auth.logout'), function()
{
    Route::match(array('GET', 'POST'), 'loginauth', 'LoginController@login');
    Route::match(array('GET', 'POST'), 'login', 'IndexController@login');
    Route::match(array('GET', 'POST'), 'forgot-password', 'ForgotPasswordController@index');
    Route::match(array('GET', 'POST'), 'change-password/{email}/{username}', 'ForgotPasswordController@changePassword');
    Route::match(array('GET', 'POST'), 'email-change-password', 'ForgotPasswordController@emailChangePassword');
    Route::match(array('GET', 'POST'), 'change-password-process', 'ForgotPasswordController@changePasswordProcess');
    Route::match(array('GET', 'POST'), '/', 'IndexController@index');
    Route::match(array('GET', 'POST'), 'about-us', 'IndexController@about');
    Route::match(array('GET', 'POST'), 'product', 'IndexController@product');
    Route::match(array('GET', 'POST'), 'marketing-plan', 'IndexController@marketing_plan');
    Route::match(array('GET', 'POST'), 'contact-us', 'IndexController@contact_us');

    Route::match(array('GET','POST'),'registration','RegistrationController@index');
    Route::match(array('GET','POST'),'registration/success','RegistrationController@success');
    Route::match(array('GET','POST'),'registration/ajax','RegistrationController@ajaxValidateRemote');
    Route::match(array('GET','POST'),'registration/login','IndexController@login');

});