<?php
/**
 * Created by PhpStorm.
 * User: larry
 * Date: 10/8/2015
 * Time: 6:44 PM
 */

define("COMMON_USER",0);
define("SUPER_ADMIN",1);
define("ADMIN",2);

define('NEW_CODE',1);
define("ACTIVE_CODE",2);
define("DISABLE_CODE",0);

define("ENTREP",1);
define("BRONZE",2);
//define("SILVER",3);
//define("GOLD",4);
define("DIAMOND",5);
define("MAX_NUMBER_ACCOUNTS",7);

define("LEFT",0);
define("RIGHT",1);

//pairing limit
define("PAIRING_LIMIT",1000);
// package type
define("SILVER",0);
define("GOLD",1);
// package points
define("BRONZE_POINTS",50);
define("SILVER_POINTS",150);

// package direct referral fee
define("ONE_FIVE_DR",200);
define("FOUR_NINE_EIGHTY_DR",400);

// Pay Out Status
define("PAID",1);
define("UNPAID",0);

// withdraw type
define("CASH",0);
define("BANK",1);
define("CHECK",2);
