{{ Theme::asset()->usePath()->add('css-login','css/login.css') }}
<div id="login-form">
{{ Form::open(array('url' => 'loginauth','name'=>'loginPage', 'id' => 'loginPage' )) }}
    <div class="container-login">
        <div class="form-header">
            <h3>User Login</h3>
        </div>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-md-3"></label>
                <div class="col-md-7">
                    <div class="error">{{ Session::get('Status') }}</div>
                     <div class="error">{{ $errors->first('username') }}</div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Username</label>
                <div class="col-md-6">
                    {{ Form::text('username', Input::old('username'), array('placeholder' => 'Username', 'id' => 'username', 'class' =>'username form-control')) }}
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Password</label>
                <div class="col-md-6">
                    <div class="formControl">{{ Form::password('password', array('placeholder' => 'Password', 'id' => 'password', 'class' =>'password form-control')) }}</div>
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3"></label>
                <div class="col-md-6">
                    <p style="color:black">Don't have an account? &nbsp;
                        <a style="text-decoration: underline" href="{{ URL::to('registration') }}" >Register</a> &nbsp;
                        <a style="text-decoration: underline" href="{{ URL::to('forgot-password') }}" >Forgot Password</a>
                    </p>

                    <input type="submit" class="btn btn-primary" value="Login" id="auth_login">
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>

<script>
/*
$('form#loginPage').validate({
    rules: {
        username: {
            required: true,
            remote: {
                url: "registration/ajax?type=check-user-name",
                type: "post",
                data: {
                    userName: function() {
                        return $( "#username" ).val();
                    },
                    userLogin: function() {
                            return 'userLoginValidation';
                    }
                },
                complete: function(data){
                    if( data.responseText == "true" )
                        $('#auth_login').attr('disabled', false);
                    else
                        $('#auth_login').attr('disabled', true);
                }

            }
        },
        password: {
            required: true
        }
    },
    messages : {
        username: {
            remote: 'Username is invalid.'
        }
    },
    submitHandler: function (form) {
        form.submit();
    },
    focusInvalid: false,
    debug: true,
    errorElement: 'div',
    ignore: '*:not([name])'
});

$('#username').on('keypress keyup', function(e) {
    if($('.col-md-7 > div.error').text() != '')
        $('.col-md-7 > div.error').hide();

    if (e.which === 32)
        return false;
})
*/
</script>