<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <br><br>
        <h1 class="header center light light-green-text text-darken-3">Mont Pierre International</h1>
        <div class="row center">
            <h5 class="header col s12 light">Help You Reach The Top</h5>
        </div>
        <div class="row center">
            <a href="{{ URL::to('marketing-plan')  }}" class="btn-large green">See How</a>
            <i>and check</i>
            <a href="{{ URL::to('product')  }}" class="btn-large orange">Our Product</a>
        </div>
    </div>
    <img src="{{ URL::to('themes/mpi/assets/img/montpierre-bg.png') }}" class="montain">
</div>

