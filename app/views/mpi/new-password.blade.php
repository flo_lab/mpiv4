

{{ Theme::asset()->usePath()->add('css-login-form','css/form-login.css') }}
<br>
<div class="main-content">
    <!-- You only need this form and the form-login.css -->


    {{ Form::open(array('url' => 'change-password-process','name'=>'change-password','id'=>'change-password', 'class' => 'form-login' )) }}
        <input type="hidden" name="email" value="{{$email}}" />
        <input type="hidden" name="username" value="{{$username}}"/>
    <div class="form-log-in-with-email">

        <div class="form-white-background">
            <?php $status=Session::get('Status'); ?>
                @if($status=='Success')
                <div class="form-title-row">
                    <h1>Change Password Successful..!</h1>
                </div>
                    <a href="{{ URL::to('login') }}" class="form-forgotten-password">Login &middot;</a>
                    <a href="{{ URL::to('registration') }}" class="form-create-an-account">Be a MPI dealer &rarr;</a>
            @else

                <div class="form-title-row">
                    <h1>New Password</h1>
                </div>
                <div class="form-row form-title-row">
                    <span class="form-title">Enter your new password</span>
                </div>
                <div class="col-md-7">
                    <div class="error">{{Session::get('Status')}}</div>
                </div><br>
                <div class="form-row">
                    <label>
                        <span>Enter New Password</span>
                        <input type="password" name="new-password" id="new-password" required="">
                    </label>
                </div>

                <div class="form-row">
                <label>
                <span>Re-Type Password</span>
                <input type="password" name="retype-password" id="retype-password" required="">
                </label>
                </div>

                <div class="form-row">
                    <button type="submit" id="submit">Submit</button>
                </div>
            @endif
        </div>

    </div>


    </form>

</div>
<script>
    $(document).ready(function() {

            $('form#change-password').submit(function(e){

                var password = $('#new-password').val();
                var retype_password = $('#retype-password').val();
                if(password!=retype_password){

                    $('.error').html('Your password do not match');
                    return false;
                }else{
                    $('#submit').prop('disabled', true);
                    return true;
                }
            });
    });
</script>

