<div class="section no-pad-bot" id="index-banner">
    <div class="">
        <div class="section">
            <div class="row">
                <div class="col s12 m2">
                    <div class="icon-block">
                        <div class="ladder-to-success-girl"><img src="{{ URL::to('uploads/ladder-to-success.png') }}"></div>
                        <h6>Ladder to Success</h6>
                        <p class="light" style="font-size: 11px">Lorem Ipsum est un générateur de faux textes aléatoires. Vous choisissez le nombre de paragraphes, de mots ou de listes.</p>
                    </div>
                </div>
                <div class="col s12 m9">
                    <div class="icon-block">
                        <div class="green lighten-4 marketing-plan-main">
                            <h3><center>Temporary Not Available</center></h3>
                            {{--<div><img src="{{ URL::to('uploads/marketing-plan/A.png') }}"></div>--}}
                            {{--<div><img src="{{ URL::to('uploads/marketing-plan/B.png') }}"></div>--}}
                            {{--<div><img src="{{ URL::to('uploads/marketing-plan/C.png') }}"></div>--}}
                            {{--<div><img src="{{ URL::to('uploads/marketing-plan/D.png') }}"></div>--}}
                            {{--<div><img src="{{ URL::to('uploads/marketing-plan/E.png') }}"></div>--}}
                            {{--<div><img src="{{ URL::to('uploads/marketing-plan/F.png') }}"></div>--}}
                            {{--<div><img src="{{ URL::to('uploads/marketing-plan/G.png') }}"></div>--}}
                            {{--<div><img src="{{ URL::to('uploads/marketing-plan/H.png') }}"></div>--}}
                            {{--<div>--}}
                                {{--<video class="step-video" width="100%" height="300" controls="">--}}
                                    {{--<source src="{{ URL::to('uploads/video.mp4') }}" type="video/mp4">--}}
                                    {{--<source src="movie.ogg" type="video/ogg">--}}
                                    {{--Your browser does not support the video tag.--}}
                                {{--</video>--}}
                            {{--</div>--}}
                        </div>
                        <div class="green lighten-4 marketing-plan-item center">
                            {{--<div class="white">Ways to earn</div>--}}
                            {{--<div class="green darken-2 white-text">Step 1</div>--}}
                            {{--<div class="orange darken-1 white-text">Step 2</div>--}}
                            {{--<div class="grey darken-2 white-text">Step 3</div>--}}
                            {{--<div class="amber accent-4 white-text">Step 4</div>--}}
                            {{--<div class="light-blue darken-2 white-text">Step 5</div>--}}
                            {{--<div class="light-blue darken-1 white-text">Step 6</div>--}}
                            {{--<div class="light-blue white-text">Step 7</div>--}}
                            {{--<div class="black white-text">Watch (100%)</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br>
</div>

<link rel="stylesheet" type="text/css" href="{{Theme::asset()->usePath()->url('css/slick-css/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{Theme::asset()->usePath()->url('css/slick-css/slick-theme.css')}}">
<script type="text/javascript" src="{{ Theme::asset()->usePath()->url('js/slick-js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ Theme::asset()->usePath()->url('js/marketing-plan.js') }}"></script>