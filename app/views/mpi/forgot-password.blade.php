

{{ Theme::asset()->usePath()->add('css-login-form','css/form-login.css') }}
<br>
<div class="main-content">
    <!-- You only need this form and the form-login.css -->


        {{ Form::open(array('url' => 'email-change-password','name'=>'change-password', 'class' => 'form-login' )) }}
        <div class="form-log-in-with-email">

            <div class="form-white-background">
                <?php $status=Session::get('Status'); ?>
                @if($status=='Success')

                        <div class="form-title-row">
                            <h1>A link sent to your email to change your password.</h1>
                        </div>

                 @else
                <div class="form-title-row">
                    <h1>Forgot Password</h1>
                </div>
                <div class="form-row form-title-row">
                <span class="form-title">Enter your MPI Email address and Username.</span>
                </div>
                <div class="col-md-7">
                    <div class="error">{{ $status }}</div>
                    <div class="error">{{ $errors->first('email') }}</div>
                    <div class="error">{{ $errors->first('username') }}</div>
                </div>
                        <div class="form-row">
                            <label>
                                <span>Username</span>
                                <input type="text" name="username">
                            </label>
                        </div>
                <div class="form-row">
                    <label>
                        <span>Email</span>
                        <input type="email" name="email">
                    </label>
                </div>

                {{--<div class="form-row">--}}
                    {{--<label>--}}
                        {{--<span>Password</span>--}}
                        {{--<input type="password" name="password">--}}
                    {{--</label>--}}
                {{--</div>--}}

                <div class="form-row">
                    <button type="submit">Submit</button>
                </div>
                @endif
            </div>

            <a href="{{ URL::to('login') }}" class="form-forgotten-password">Login &middot;</a>
            <a href="{{ URL::to('registration') }}" class="form-create-an-account">Be a MPI dealer &rarr;</a>

        </div>

        {{--<div class="form-sign-in-with-social">--}}

            {{--<div class="form-row form-title-row">--}}
                {{--<span class="form-title">Sign in with</span>--}}
            {{--</div>--}}

            {{--<a href="#" class="form-google-button">Google</a>--}}
            {{--<a href="#" class="form-facebook-button">Facebook</a>--}}
            {{--<a href="#" class="form-twitter-button">Twitter</a>--}}

        {{--</div>--}}

    </form>

</div>


