<div class="section no-pad-bot" id="index-banner">
    <div class="">
        <div class="section">
            <div class="row">
                <div class="col s12 m2">
                    <div class="icon-block">
                        <div class="mont-green-iso"><img src="{{ URL::to('uploads/product/mont-green-iso.png') }}"></div>
                        <h6>Clemen</h6>
                        <p class="light" style="font-size: 11px">Lorem Ipsum est un générateur de faux textes aléatoires. Vous choisissez le nombre de paragraphes, de mots ou de listes.</p>
                    </div>
                </div>
                <div class="col s12 m8">
                    <div class="icon-block">
                        <br>
                        <div class="light-green lighten-4 mont-prod-main">
                            <div><img src="{{ URL::to('uploads/product/kiosk_design.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/combo.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/mont-green.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/mont-black.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/mont-pink.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/mont-red.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/a.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/b.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/c.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/d.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/e.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/f.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/product_new_1.jpg') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/product_new_2.jpg') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/product_new_3.jpg') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/product_new_4.jpg') }}"></div>
                        </div>

                        <div class="light-green lighten-4 mont-prod-item">
                            <div><img src="{{ URL::to('uploads/product/kiosk_design.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/combo.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/mont-green.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/mont-black.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/mont-pink.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/mont-red.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/a.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/b.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/c.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/d.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/e.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/f.png') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/product_new_1.jpg') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/product_new_2.jpg') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/product_new_3.jpg') }}"></div>
                            <div><img src="{{ URL::to('uploads/product/product_new_4.jpg') }}"></div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m2">
                    <div class="icon-block">
                        <div class="mont-pink-iso"><img src="{{ URL::to('uploads/product/mont-pink-iso.png') }}"></div>
                        <h6>Ellyse</h6>
                        <p class="light" style="font-size: 11px">Lorem Ipsum est un générateur de faux textes aléatoires. Vous choisissez le nombre de paragraphes, de mots ou de listes.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br><br>
    {{--<div class="container"></div>--}}
</div>

<link rel="stylesheet" type="text/css" href="{{Theme::asset()->usePath()->url('css/slick-css/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{Theme::asset()->usePath()->url('css/slick-css/slick-theme.css')}}">
<script type="text/javascript" src="{{ Theme::asset()->usePath()->url('js/slick-js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ Theme::asset()->usePath()->url('js/product.js') }}"></script>