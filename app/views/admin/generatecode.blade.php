<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<img src="{{Theme::asset()->url('img/new_logo.png')}}" width="80" height="50"><br>
<label style="font-family: Arial;font-size: 12px;">Mont Pierre International</label>
<label style="font-family: Arial;font-size: 16px;margin-left:160px;">Code Release</label>
<div>
    <br>
    <table style="border:solid 1px;width: 100%;">
        <tr style="background-color:#d6e9c6">
            <th>Id</th>
            <th>Control No.</th>
            <th>Security No.</th>
            <th>Date</th>
            <th>Status</th>
            <th>Package</th>
            <th>Code Generated by</th>

        </tr>

        @foreach($gencode as $key => $val)

      <tr>
          <td>{{$val['id']}}</td>
          <td>{{$val['code']}}</td>
          <td>{{$val['security']}}</td>
          <td>{{$val['date']}}</td>
          @if($val['status']=="Used")
             <td style="color:red">{{$val['status']}}</td>
          @else
             <td>{{$val['status']}}</td>
          @endif
          @if($val['package']=="Gold")
              <td style="color:gold">{{$val['package']}}</td>
          @else
              <td style="color:sandybrown">{{$val['package']}}</td>
          @endif

          <td>{{$val['user']}}</td>
      </tr>
        @endforeach
    </table>
</div>

