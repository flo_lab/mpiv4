<script>
    $(document).ready(function(){
        $('#payoutlist').dataTable({
            "scrollCollapse": true,
            "sDom": '<"top"f>rt<"bottom"ip><"clear">',
            "bServerSide": true,
            "sAjaxSource": "ajax?type=payout-list"
        });
        $('#confirm-cashout').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

            $('#user-withdraw-id').val($(e.relatedTarget).data('id'));
            $('#userId').val($(e.relatedTarget).data('href'));

            if($(e.relatedTarget).data('type')==0){
                $('.form').hide();
                $('#account-no').val('');
                $('#account-name').val('');
            }else if($(e.relatedTarget).data('type')==1){
                $('.form').show();
                $('#account-no').val($(e.relatedTarget).data('account-no'));
                $('#account-name').val($(e.relatedTarget).data('account-name'));
            }else{
                $('.form').hide();
                $('#account-no').val('');
                $('#account-name').val('');
            }
        });

        $('.btn-cashout').on('click', function(e) {
            $(this).attr('disabled','true');
            $.ajax({
                url:"admin-cashout",
                type: "POST",
                data : {
                    id:$('#user-withdraw-id').val(),
                    userId:$('#userId').val()
                },
                success: function(data, textStatus, jqXHR)
                {
                    location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                }
            });
        });
    });
</script>
<table id="payoutlist" class="table" style="margin:10px">
    <thead style="font-size: 14px; background: radial-gradient(#d1f1de, #b7fdd4); background: -webkit-radial-gradient(#e7eff1, #fdf9fb)">
    <tr>
        <th class="border-table">Dealer ID</th>
        <th class="border-table">Dealer Name</th>
        <th class="border-table">Amount</th>
        <th class="border-table">Withdraw Date</th>
        <th class="border-table">Type</th>
        <th class="border-table">Action</th>
    </tr>
    </thead>
</table>

<div class="modal fade" id="confirm-cashout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <label for="account-name">Confirm Pay Out</label>
            </div>
            <div class="modal-body">
                <form role="form" class="form">
                    <input type="hidden" class="form-control" id="user-withdraw-id" readonly>
                    <input type="hidden" class="form-control" id="userId" readonly>
                    <div class="form-group">
                        <label for="account-no">Bank Account Number</label>
                        <input type="text" class="form-control" id="account-no" readonly>
                    </div>
                    <div class="form-group">
                        <label for="account-name">Account Name</label>
                        <input type="text" class="form-control" id="account-name" readonly>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary btn-cashout">Cashout</a>
            </div>
        </div>
    </div>
</div>