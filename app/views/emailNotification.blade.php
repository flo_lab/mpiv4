<div style="margin-left:400px;">
    <h4>MontPierre International</h4></div>
<p style="text-align:left;font-size:14px;">Hi&nbsp;<strong>{{ $emailTo }}</strong>,<br/><br/>
    Click the link below to change your password account.<br/><br/>

    <a href="{{ URL::to('change-password/'.$emailTo.'/'.$encryptedParam) }}"><div class="sideBarMenu"><i class="glyphicon glyphicon-log-out">&nbsp;</i>Click This Link To Change Password</div></a>
    <br/><br/>
    <strong>Montpierre International</strong><br/>

    <a href="http://mymontpierre.net/">mymontpierre.net/</a><br/><br/></p>
    <p style="font-size:12px;">This email message and accompanying data may contain information that is confidential
    and subject to privilege and privacy laws. If you are not the intended recipient,
    you are notified that any use, dissemination, distribution or copying of
    this message or data is prohibited. If you have received this email in
    error please notify us immediately and delete all material pertaining to this email.
    Unless otherwise agreed in writing, by engaging a person or contractor referred
    to you by us, you acknowledge and agree to accept Mont Pierre International
    terms and conditions from whom such person was referred.
    A copy of such terms is available from us.</p>