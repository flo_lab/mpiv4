{{ Theme::asset()->usePath()->add('js-registration','js/registration.js') }}
{{ Theme::asset()->usePath()->add('css-registration','css/registration.css') }}
<div id="reg-form">
    {{ Form::open(array('url' => 'registration','name'=>'registrationForm', 'id' => 'registrationForm' )) }}
    <div class="container-regs">
        <div class="form-header">
            <h3>Membership Information</h3>
        </div>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-md-7"><div class="span12"><span style="color:red">{{Session::get('status')}}</span></div></label>
            </div>
            <div class="form-group">
                <label class="col-md-3">Control Number</label>
                <div class="col-md-6">
                    <input type="text" class="form-control alphaNumeric" id="genCode" name="genCode" placeholder="Control Number" maxlength="9">
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Security Code</label>
                <div class="col-md-6">
                    <input type="text" disabled class="form-control alphaNumeric" id="securityCode" name="securityCode" placeholder="Security Code" maxlength="9">
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Sponsor ID</label>
                <div class="col-md-6">
                    <input type="text" class="form-control numberOnly" id="sponsor" name="sponsor" placeholder="Sponsor ID" maxlength="12" value="{{Input::old('sponsor')}}">
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Placement ID</label>
                <div class="col-md-6">
                    <input type="text" class="form-control numberOnly" id="placement" name="placement" placeholder="Placement ID" maxlength="12" value="{{Input::old('placement')}}">
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3">Position</label>
                <div class="col-md-6">
                    <select class="form-control" id="position" name="position">
                        <option value="" selected="selected">Select Position</option>
                        <option value="0">Left</option>
                        <option value="1">Right</option>
                    </select>
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
        </div>
    </div>
    <div class="container-regs">
        <div class="form-header">
            <h3>Personal Information</h3>
        </div>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-md-3">Last Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control alphaOnly auto-cap" id="lastname " name="lastname" placeholder="Last Name" maxlength="50" value="{{Input::old('lastname')}}" >
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">First Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control alphaOnly auto-cap" id="firstname" name="firstname" placeholder="First Name" maxlength="50" value="{{Input::old('firstname')}}">
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Middle Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control alphaOnly auto-cap" id="middlename" name="middlename" placeholder="Middle Name" maxlength="50" value="{{Input::old('firstname')}}">
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Contact No</label>
                <div class="col-md-6">
                    <input type="text" class="form-control numberOnly" id="contact" name="contact" placeholder="Contact No." maxlength="25" value="{{Input::old('contact')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Email Address</label>
                <div class="col-md-6">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" maxlength="25" value="{{Input::old('email')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Address</label>
                <div class="col-md-6">
                    <input type="text" class="form-control alphaNumeric" id="address" name="address" placeholder="Address" maxlength="150" value="{{Input::old('address')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Tin Number</label>
                <div class="col-md-6">
                    <input type="text" class="form-control numberOnly" id="tin" name="tin" placeholder="Tin Number" maxlength="25" value="{{Input::old('tin')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Birthdate</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="birthdate" name="birthdate" placeholder="Birthdate" value="{{Input::old('birthdate')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="container-regs">
        <div class="form-header">
            <h3>Log-In Information</h3>
        </div>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-md-3">Username</label>
                <div class="col-md-6">
                    <input type="text" class="form-control alphaNum" id="userName" name="userName" maxlength="25" value="{{Input::old('userName')}}">
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Password</label>
                <div class="col-md-6">
                    <input type="password" class="form-control alphaNumeric" id="userPassword" name="userPassword" maxlength="50">
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Confirm Password</label>
                <div class="col-md-6">
                    <input type="password" class="form-control alphaNumeric" id="userConfirmPassword" name="userConfirmPassword" maxlength="50">
                </div>
                <div>
                    <span class="asterisk">*</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3"></label>
                <div class="col-md-7">
                        <input required="true" type="checkbox" name="iagree" id="iagree">
                        <a href="javascript: void(0);" id="termsAnchor" style="text-decoration:underline">Terms and Conditions.</a>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3"></label>
                <div class="col-md-7">
                    <button class="btn btn-primary" @if(!Input::old('genCode')) disabled @endif id="register">Register</button>
                </div>
            </div>
        </div>
    </div>

    <div id="contentModal">

    </div>

    <div id="modalWholeContentTerms" hidden>
        <div id="contentModalTerms">
            <div>
                <h4>INDEPENDENT MONT PIERRE INTERNATIONAL (MPI) DISTRIBUTOR APPLICATION AND AGREEMENT TERMS AND CONDITIONS</h4>
                <p>
                    As used throughout these terms and conditions, “Agreement” collectively refers to the Mont Pierre International Independent Distributor Application and Agreement, Policies and Procedures, Marketing and Compensation Plan, and any other document incorporated by reference in the aforesaid. These documents, in their current form, and as may be amended by MPI at its sole discretion, constitute the entire contract between MPI and the MPI Distributor. No other representation, promise, or agreement, shall be binding on the parties unless in writing and signed by an authorized officer of Mont Pierre International (MPI).
                </p>
                <p>
                    1. I certify that I am at least 18 years old (or of contractual age in my state of legal residence) and that all information I provided on this Application is accurate. I understand Mont Pierre International (MPI) requests my personal tax identification number solely for the purpose of reporting income to the Bureau of Internal Revenue (BIR). Under its right of contract, Mont Pierre International (MPI) maintains the right to decline my application if I do not provide a valid tax identification number. If I am below 18 years of age, I understand that I need to provide a waiver from my parent(s) or legal guardian(s) stating that the latter takes full responsibility on the consequences of acts to be done by me as an MPI Distributor, thereby waiving any liability from MPI, in cases of incurring such liability to third party.
                </p>
                <p>
                    2. I understand that I am not required to make any product purchases in exchange for the right to distribute Mont Pierre International (MPI) products pursuant to this Agreement, with the exception of a required Business Kit, which is non-commissionable, and provided at cost.
                </p>
                <p>
                    3. I understand that Mont Pierre International (MPI) does not accept initial applications from business entities. Once my personal application is accepted, I will have the option to add a business entity to my account in accordance with the Policies and Procedures.
                </p>
                <p>
                    4. I understand that as an Mont Pierre International (MPI) Distributor: a) I am granted the non-exclusive right to offer for sale Mont Pierre International (MPI) products and services in accordance with the Agreement, b) I have the right to enroll persons in Mont Pierre International (MPI), and c) If qualified, I have the right to earn commissions pursuant to the Mont Pierre International (MPI) Compensation Plan.
                </p>
                <p>
                    5. I agree to abide by the Mont Pierre International (MPI) Code of Conduct as detailed in the Mont Pierre International (MPI) Policies and Procedures.
                </p>
                <p>
                    6. I agree to present the Mont Pierre International (MPI) Marketing and Compensation Plan and Mont Pierre International (MPI) products and services as set forth in literature that is officially produced by Mont Pierre International (MPI).
                </p>
                <p>
                    7. I agree that as a Mont Pierre International (MPI) Distributor I am an independent contractor, and not an employee, partner, legal representative, or franchisee of Mont Pierre International (MPI). I understand and agree that I will be solely responsible for paying all expenses incurred by myself, including but not limited to travel, food, lodging, secretarial, office, long distance telephone and other expenses. I UNDERSTAND THAT I SHALL NOT BE TREATED AS AN EMPLOYEE OF MONT PIERRE INTERNATIONAL (MPI) FOR ANY PURPOSE. Mont Pierre International (MPI) is not responsible for tax withholding, and reserves the right to refuse to withhold or deduct from my bonuses and commissions, if any, FICA or taxes of any kind, even if requested or agreed to by me in order to comply with any governmental order of backup withholding. I understand I am responsible to pay all applicable taxes and/or license fees, and workers compensation taxes that may become due as a result of my activities as an Independent Distributor.
                </p>
                <p>
                    8. If eligible, I will be compensated for the products I sell and those products sold through my sales organization. Mont Pierre International (MPI) never compensates for the mere act of sponsoring. The sale of products to end consumers must be emphasized in all presentations.
                </p>
                <p>
                    9. I have carefully read and agree to comply with the Mont Pierre International (MPI) Agreement. I understand that I must be in good standing, and not in violation of the Agreement, to be eligible for bonuses or commissions from Mont Pierre International (MPI). I understand that these Terms and Conditions, the Mont Pierre International (MPI) Policies and Procedures, or the Mont Pierre International (MPI) Marketing and Compensation Plan may be amended at the sole discretion of Mont Pierre International (MPI), and by submitting this application I agree to abide by all such amendments. Amendments shall be binding immediately after notification is released. The continuation of my Mont Pierre International (MPI) business or my acceptance of bonuses or commissions shall constitute my acceptance of any and all amendments.
                </p>
                <p>
                    10. I may not assign any rights or delegate my duties under the Agreement without the prior written consent of Mont Pierre International (MPI). Any attempt to transfer or assign the Agreement without the express written consent of Mont Pierre International (MPI) renders the Agreement voidable at the option of Mont Pierre International (MPI) and may result in termination of my business.
                </p>
                <p>
                    11. I understand that if I fail to comply with any of the terms of the Agreement, Mont Pierre International (MPI) may, at its discretion, impose upon me disciplinary action as set forth in the Policies and Procedures, up to and including the termination of my Distributorship. If I am in breach, default or violation of the Agreement at termination, I shall not be entitled to receive any further bonuses or commissions, whether or not the sales for such bonuses or commissions have been completed.
                </p>
                <p>
                    12. I represent and warrant that my participation as an Mont Pierre International (MPI) Distributor does not breach, violate, or otherwise interfere with any current agreements, past agreements, or surviving clauses of previous agreements, into which I have entered with any other multi-level marketing, direct sales, or other business venture.
                </p>
                <p>
                    13. I understand and agree that if I elect to participate in another non-competing multi-level marketing or direct sales opportunity, I will maintain separate organizations, independent of one-another, for each such non-competing opportunity.
                </p>
                <p>
                    14. Mont Pierre International (MPI)  , its parent or affiliated companies, directors, officers, shareholders, employees, assigns, and agents (collectively referred to as “Agents”), shall not be liable for, and I release Mont Pierre International (MPI)   and its Agents from, all claims for consequential and exemplary damages for any claim or cause of action relating to the Agreement. I further agree to release Mont Pierre International (MPI)  and its Agents from all liability arising from or relating to the promotion or operation of my Mont Pierre International (MPI)  business and any activities related to it (e.g., the presentation of Mont Pierre International (MPI)   products or Compensation and Marketing Plan, the operation of a motor vehicle, the lease of meeting or training facilities, etc.), and agree to indemnify Mont Pierre International (MPI)  for any liability, damages, fines, penalties, or other awards arising from any unauthorized conduct that I undertake in operating my business.
                </p>
                <p>
                    15. The Agreement, in its current form and as amended by Mont Pierre International (MPI) at its discretion, constitutes the entire contract between Mont Pierre International (MPI) and myself. Any promises, representations, offers, or other communications not expressly set forth in the Agreement are of no force or effect.
                </p>
                <p>
                    16. Any waiver by Mont Pierre International (MPI) of any breach of the Agreement must be in writing and signed by an authorized officer of Mont Pierre International (MPI). Waiver by Mont Pierre International (MPI) of any breach of the Agreement by me shall not operate or be construed as a waiver of any subsequent breach.
                </p>
                <p>
                    17. If any provision of the Agreement is held to be invalid or unenforceable, such provision shall be reformed only to the extent necessary to make it enforceable and the balance of the Agreement will remain in full force and effect.
                </p>
                <p>
                    18. This Agreement will be governed by and construed in accordance with the laws of the State of Washington without regard to principles of conflicts of laws. In the event of a dispute between an Independent Distributor and Mont Pierre International (MPI) arising from or relating to the Agreement, or the rights and obligations of either party, the parties shall attempt in good faith to resolve the dispute through nonbinding mediation as more fully described in the Policies and Procedures. Mont Pierre International (MPI) shall not be obligated to engage in mediation as a prerequisite to disciplinary action against an Independent Distributor. If the parties are unsuccessful in resolving their dispute through mediation, the dispute shall be settled totally and finally by arbitration as more fully described in the Policies and Procedures.
                </p>
                <p>
                    19. If an Independent Distributor wishes to bring an action against Mont Pierre International (MPI) for any act or omission relating to or arising from the Agreement, such action must be brought within one year from the date of the alleged conduct giving rise to the cause of action. Failure to bring such action within such time shall bar all claims against Mont Pierre International (MPI) for such act or omission. Distributor waives all claims that any other statute of limitations applies.
                </p>
                <p>
                    20. I authorize Mont Pierre International (MPI) to use my name, photograph, personal story and/or likeness in advertising or promotional materials and waive all claims for remuneration for such use.
                </p>
                <p>
                    21. I understand that I cannot, under any circumstances, incur any debt, expense, or obligation on behalf of, or for, the Company.
                </p>
                <p>
                    22. The Company reserves the right to accept or reject any applicant and is under no obligation to offer any reason for rejection. The Company is under no obligation to notify an applicant of an incomplete or faulty application.
                </p>
                <p>
                    23. A faxed or online copy of this Agreement shall be treated as an original in all respects.
                </p>
                <h4>RETURN / REFUND POLICY</h4>
                <p>
                    The goal of Mont Pierre International (MPI) is to ensure that Customers are completely satisfied with their purchases. Upon receipt of any package, it should be examined closely prior to opening the products’ packaging. If the order is not in good condition, it may be returned provided that the following guidelines are observed:
                    Returns for “refund” are accepted by the Company only if submitted within 7 days from the purchase date along with the receipt. Returns of product for the purposes of “exchange” for other product are accepted by the Company if submitted within 7 days from the purchase date. Returns submitted to Mont Pierre International (MPI) after 7 days will only be processed at the discretion of the Company. If there is any concern about return timelines, the Customer is encouraged to contact the Company directly as soon as possible.
                    All products returned (other than for determination of Defects, as outlined below), must be returned unused, unopened and in resalable condition.
                </p>
                <h4>Website Policies, Modifications & Severability</h4>
                <p>
                    Please review our other policies, such as our privacy policy and exchange policy posted on this website. These policies also govern your visit to mymontpierre.net. We reserve the right to make changes to our website, policies, and these Conditions of Use at any time. If any of these conditions shall be deemed invalid, void, or for any reason unenforceable, that condition shall be deemed severable and shall not affect the validity and enforceability of any remaining condition.
                </p>
                <p>
                    DISCLAIMER OF WARRANTIES AND LIMITATION OF LIABILITY THIS WEBSITE AND ALL INFORMATION, CONTENT, MATERIALS, PRODUCTS (INCLUDING SOFTWARE) AND SERVICES INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THIS WEBSITE ARE PROVIDED BY MONT PIERRE INTERNATIONAL (MPI) ON AN “AS IS” AND “AS AVAILABLE” BASIS, UNLESS OTHERWISE SPECIFIED IN WRITING MONT PIERRE INTERNATIONAL (MPI) MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THIS WEBSITE OR THE INFORMATION, CONTENT, MATERIALS, PRODUCTS (INCLUDING SOFTWARE) OR SERVICES INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THIS WEBSITE, UNLESS OTHERWISE SPECIFIED IN WRITING. YOU EXPRESSLY AGREE THAT YOUR USE OF THIS WEBSITE IS AT YOUR SOLE RISK.
                    TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, MONT PIERRE NTERNATIONAL (MPI) DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. MONT PIERRE NTERNATIONAL (MPI) DOES NOT WARRANT THAT THIS WEBSITE; INFORMATION, CONTENT, MATERIALS, PRODUCTS (INCLUDING SOFTWARE) OR SERVICES INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THIS WEBSITE; THEIR SERVERS; OR E-MAIL SENT FROM MONT PIERRE INTERNATIONAL (MPI) ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. MONT PIERRE INTERNATIONAL (MPI) WILL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING FROM THE USE OF THIS WEBSITE OR FROM ANY INFORMATION, CONTENT, MATERIALS, PRODUCTS (INCLUDING SOFTWARE) OR SERVICES INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THIS WEBSITE, INCLUDING, BUT NOT LIMITED TO DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, AND CONSEQUENTIAL DAMAGES, UNLESS OTHERWISE SPECIFIED IN WRITING. CERTAIN STATE LAWS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME OR ALL OF THE ABOVE DISCLAIMERS, EXCLUSIONS, OR LIMITATIONS MAY NOT APPLY TO YOU, AND YOU MIGHT HAVE ADDITIONAL RIGHTS, WHICH SHALL BE RAISED WITHIN THE PRESCRIBED PERIOD STIPULATED IN THE TERMS AND CONDITIONS OF THE COMPANY AS PROVIDED UPON SIGNING UP AS AN MPI DISTRIBUTOR, OTHERWISE YOU ARE ESTOPPED FROM CLAIMING SUCH RIGHT.
                </p>
            </div>
            <span>&nbsp;I have read and agree to the Terms and Conditions.</span>
        </div>

        <div id="footerModalTerms">
            <a id="iagree-btn" class="btn btn-primary">ACCEPT</a>
        </div>
    </div>

    {{Form::close()}}
</div>