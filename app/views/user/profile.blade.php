<style>
    td{
        margin: 0;
        padding-bottom: 0;
    }
    input[type='text']{
        margin: 0;
    }
    #modalChangePassword{
        padding: 20px
    }
    #modalChangePassword h5{
        color:#c7c6c6;font-size: 25px;margin-bottom: 20px;
    }
    #modalChangePassword form > div{
        margin-bottom: 10px
    }
    .modalClose{
        border: 1px solid #0f0c55;border-radius: 5px;font-weight: 500;
        color:#0f0c55;background: transparent;float: right;    float: right;width: 170px;
        text-align: center;cursor: pointer;
    }
    .modalSave{
        float: right;margin-right: 10px;border: 1px solid #0f0c55;border-radius: 5px;font-weight: 500;
        color:#0f0c55;background: transparent;float: right;    float: right;width: 170px;
    }
    .custom-btn{
        border: 1px solid #0f0c55;border-radius: 5px;font-weight: 500;
        color:#0f0c55;background: transparent;width: 156px
    }
    .container-profile{
        padding: 20px;padding-top: 0
    }
    .container-profile > h5{
        color: rgb(0, 0, 0);
        font-size: 19px;
        font-weight: 500;
    }
    .avatar{
        border: 1px solid #ddd;position: absolute;left: 0;
        right: 0;top: 0;
        bottom: 0;
        margin: auto
    }
    .lbl-grey{
        color: #9e9e9e;
    }
    .modalOpen{
        border: 1px solid #0f0c55;border-radius: 5px;font-weight: 500;
        color:#0f0c55;background: transparent;float: right; width: 170px;
    }
    .div-1{
        box-shadow: 0px 2px 1px 1px #c6c6c6;margin-right: 10px;padding-top: 40px;padding-bottom: 60px;padding-left: 20px;
        padding-right: 20px;
    }
    .div-1 h5{
        font-weight: 500;
        color: #dcdcdc;
        font-size: 28px;
    }
</style>

<script>
    $(document).ready(function(){
        $('.modalOpen').click(function(){
            $('#modalChangePassword').openModal();
        });

        $('.modalClose').click(function(){
            $('#modalChangePassword').closeModal();
        });
    });
</script>
<div id="modalChangePassword" class="modal">
    <form id="" novalidate="novalidate">
        <h5>CHANGE PASSWORD</h5>
        <div class="clearfix">
            <div>Currently Password</div>
            <input type="password" name="currrentPassword" aria-required="true">
        </div>
        <div class="clearfix">
            <div>New Password</div>
            <input type="password" name="newPassword" id="newPassword" aria-required="true">
        </div>
        <div class="clearfix">
            <div>Confirm Password</div>
            <input type="password" name="confirmPassword" aria-required="true">
        </div>
        <div style="margin-top:50px">
            <span class="modalClose">CANCEL</span>
            <button class="modalSave">SAVE</button>
        </div>
    </form>
</div>
<div class="container-profile">
    <h5 style="margin-bottom: 6em;">My Profile</h5>
    <div class="row">
        <div class="col-md-2" style="position: relative">
            <img src="http://localhost/mpiv4/public/themes/mpi/assets/img/logo.png" width="150" height="150" class="avatar">
        </div>
        <div class="col-md-10">
            <table>
                <tr>
                    <td  width="100" class="lbl-grey">First Name</td>
                    <td>
                        <input type="text" value="{{ ucfirst($user->user_fname)  }}" disabled>
                    </td>

                    <td width="100" class="lbl-grey">Middle Name</td>
                    <td>
                        <input type="text" value="{{ ucfirst($user->user_mname)  }}" disabled>
                    </td>

                    <td width="100" class="lbl-grey">Last Name</td>
                    <td>
                        <input type="text" value="{{ ucfirst($user->user_lname)  }}" disabled>
                    </td>
                    <td>
                        <button class="modalOpen">CHANGE PASSWORD</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row div-1">
        <div class="col-md-6">
            <h5>ACCOUNT INFO</h5>
            <table>
                <tr>
                    <td width="140" class="lbl-grey">User ID</td>
                    <td>
                        <input type="text" value="{{ $user->user_dealer_id }}" disabled>
                    </td>
                </tr>
                <tr>
                    <td  class="lbl-grey">Sponsor ID</td>
                    <td>
                        <input type="text" value="{{ $user->user_sponsor }}" disabled>
                    </td>
                </tr>
                <tr>
                    <td  class="lbl-grey">Placement ID</td>
                    <td>
                        <input type="text" value="{{ $user->user_placement }}" disabled>
                    </td>
                </tr>
                <tr>
                    <td  class="lbl-grey">Position</td>
                    <td>
                        <input type="text" value="{{ ($user->user_placement_position==0) ? 'L' : 'R'; }}" disabled>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <h5>CONTACTS</h5>
            <table>
                <tr>
                    <td  width="140"  class="lbl-grey">Contact Number</td>
                    <td>
                        <input type="text" value="{{ $user->user_contact_no }}">
                    </td>
                </tr>
                <tr>
                    <td  class="lbl-grey">Email Address</td>
                    <td>
                        <input type="text" value="{{ $user->user_email }}">
                    </td>
                </tr>
            </table>
            <br>
            <h5>OTHER INFO</h5>
            <table>
                <tr>
                    <td  width="140" class="lbl-grey">Address</td>
                    <td>
                        <input type="text" value="{{ $user->user_address }}">
                    </td>
                </tr>
                <tr>
                    <td class="lbl-grey">Birthdate</td>
                    <td>
                        <input type="date" type="text" value="{{ $user->user_birthdate }}">
                    </td>
                </tr>
                <tr>
                    <td class="lbl-grey">Tin Number</td>
                    <td>
                        <input type="text" value="{{ $user->user_tin_no }}">
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div style="float: right;padding-right: 10px;">
            <button class="custom-btn">CANCEL</button>
            <button class="custom-btn">SAVE</button>
        </div>
    </div>
</div>