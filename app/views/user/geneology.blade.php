<link rel="stylesheet" type="text/css" href="{{Theme::asset()->usePath()->url('css/user-dashboard/geneology.css')}}">
<script src="{{Theme::asset()->usePath()->url('js/user-dashboard/geneology.js')}}"></script>

<div class="index-header-info">
    <div>
        <a class="dealerNo" href="#" data-downline="{{Auth::User()->user_dealer_id}}" data-status="{{Auth::User()->user_status}}">{{Auth::User()->user_dealer_id}}</a>
    </div>
    <div>{{ ucfirst(Auth::User()->user_fname).' '.ucfirst(Auth::User()->user_mname).' '.ucfirst(Auth::User()->user_lname)}}</div>
</div>
{{--<div class="zoom">--}}
    {{--Resize Chart in--}}
    {{--<button class="btn green">100%</button>--}}
    {{--<button class="btn green">80%</button>--}}
    {{--<button class="btn green">50%</button>--}}
    {{--<button class="btn green">25%</button>--}}
{{--</div>--}}

<div id="geneology">
    {{ Form::open(array('url' => 'user/geneology','name'=>'dealer', 'id' => 'dealer' )) }}
    <input type="hidden" name="collectionDealer" id="collectionDealerTemp"/>
    <input type="hidden" name="collectionStatus" id="collectionStatusTemp"/>
    <input type="hidden" name="dealerId" id="dealerId" />
    <input type="hidden" name="dealerStatus" id="dealerStatus" />
    {{Form::close()}}

    {{ Form::open(array('url' => 'user/geneology','name'=>'Auth', 'id' => 'Auth' )) }}
    <input type="hidden" name="dealerId" id="dealerId" value="{{Auth::User()->user_dealer_id}}"/>
    <input type="hidden" name="dealerStatus" id="dealerStatus" value="{{Auth::User()->user_status}}" />
    {{Form::close()}}

    <?php $class='';
    $class = GeneologyHelper::headerStatus($headStatus);
    ?>
    <div class="one {{$class}}" >
        <label class="tooltipped" data-position="top" data-delay="50" data-tooltip="{{ $head_name->user_lname . ' ' . $head_name->user_fname . ' ' . $head_name->user_mname }}"> <a class="dealerNo" href="#" data-downline="{{$head}}" data-status="{{$headStatus}}">{{$head}}</a></label>
        <span><div class="{{$class}}">{{ $head_name->user_username }}</div></span>
    </div>

    @foreach($first as $key=>$v)
        <?php $classFirst='';
        $classFirst = GeneologyHelper::headerStatus($v['status']);
        ?>

        <div class="two {{$classFirst}}">
            <label class="tooltipped" data-position="top" data-delay="50" data-tooltip="{{ $v['name'] }}"><a class="dealerNo" href="#" data-downline="{{$v['downline']}}" data-status="{{$v['status']}}">{{$v['downline']}}</a></label>
            <span><div class="{{$classFirst}}">{{ $v['username'] }}</div></span>
        </div>
    @endforeach

    @foreach($second as $key=>$val)
        <?php $classSecond = GeneologyHelper::headerStatus($val['status']); ?>

    <div class="four {{$classSecond}}">
        <label class="tooltipped" data-position="top" data-delay="50" data-tooltip="{{ $val['name'] }}"><a class="dealerNo" href="#" data-downline="{{$val['downline']}}" data-status="{{$val['status']}}">{{$val['downline']}}</a></label>
        <span style="border-top: 1px solid white; border-left: 1px solid white;border-right: 1px solid white;"><div class="{{$classSecond}}">{{ $val['username'] }}</div></span>
    </div>
    @endforeach

    <div class="" style="border:solid 1px;width: 50%;top: 30px;">
        <h4 style="background: rgba(121, 147, 97, 47); margin: 0.14rem 0 0.912rem;font-size: 1.5rem; color:white ">Total Left Dealers</h4>
        <h5>{{$left_downline}}</h5>
    </div>
    <div class="" style="border:solid 1px;width: 50%;top: 30px;">
        <h4 style="background: rgba(121, 147, 97, 47) ;margin: 0.14rem 0 0.912rem;font-size: 1.5rem;color:white">Total Right Dealers</h4>
        <h5>{{$right_downline}}</h5>
    </div>

</div>
