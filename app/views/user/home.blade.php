<link rel="stylesheet" type="text/css" href="{{Theme::asset()->usePath()->url('css/user-dashboard/home.css')}}">
<link rel="stylesheet" type="text/css" href="{{Theme::asset()->usePath()->url('css/bootstrap.min.css')}}">
<div id="content">

    <div class="col-md-4 form-white-background" style="border:solid 0px;height: 485px">
        <table style="margin:10px">
            <thead style="font-size: 14px; background: radial-gradient(#d1f1de, #b7fdd4); background: -webkit-radial-gradient(#e7eff1, #fdf9fb)">
            <tr>
                <th class="border-table" style=" text-align: right;">Profile &nbsp;</th>
                <th class="border-table">Information</th>
            </tr>

            </thead>
            <tr>
                <td>First Name:</td>
                <td>{{Auth::User()->user_fname}}</td>
            </tr>
            <tr>
                <td>Last Name:</td>
                <td>{{Auth::User()->user_lname}}</td>
            </tr>
            <tr>
                <td>M I:</td>
                <td>{{Auth::User()->user_mname}}</td>
            </tr>
            <tr>
                <td>Package:</td>
                <td>
                    {{Auth::User()->user_package_type}}
                </td>
            </tr>
            <tr>
                <td>Sponsor:</td>
                <td>{{Auth::User()->user_sponsor}}</td>
            </tr>
            <tr>
                <td>Placement:</td>
                <td>{{Auth::User()->user_placement}}</td>
            </tr>
            <thead style="font-size: 14px; background: radial-gradient(#d1f1de, #b7fdd4); background: -webkit-radial-gradient(#e7eff1, #fdf9fb)">
            <tr>
                <th class="border-table" style=" text-align: right;">Contact &nbsp;</th>
                <th class="border-table">Information
                    <a href="{{ URL::to('user/profile') }}" id="edit-contact">
                        <i class="glyphicon glyphicon-pencil" style=" float:right;margin-right: 15px;">Edit</i>
                    </a>
                </th>
            </tr>
            </thead>
            <tr>
                <td>Email:</td>
                <td>{{Auth::User()->user_email}}</td>
            </tr>
            <tr>
                <td>Contact No:</td>
                <td>{{Auth::User()->user_contact_no}}</td>
            </tr>
            <thead style="font-size: 14px; background: radial-gradient(#d1f1de, #b7fdd4); background: -webkit-radial-gradient(#e7eff1, #fdf9fb)">
            <tr>
                <th class="border-table" style=" text-align: right;">Access &nbsp;</th>
                <th class="border-table">Information
                    <a href="#" id="edit-access">
                    <i class="glyphicon glyphicon-pencil" style=" float:right;margin-right: 15px;">Edit</i>
                    </a>
                </th>
            </tr>
            </thead>
            <tr>
                <td>Username:</td>
                <td>{{Auth::User()->user_username}}</td>
            </tr>
            <tr>
                <td>Password:</td>
                <td>*********</td>
            </tr>
        </table>

    </div>
    <div class="col-md-8" style="border:solid 0px;height: 485px">
        <table id="downlineList" class="table" style="margin:10px">
            <thead style="font-size: 14px; background: radial-gradient(#d1f1de, #b7fdd4); background: -webkit-radial-gradient(#e7eff1, #fdf9fb)">
            <tr>
                <th class="border-table">Dealer ID</th>
                <th class="border-table">First Name</th>
                <th class="border-table">Last Name</th>
                <th class="border-table">M. I</th>
                <th class="border-table">Package Type</th>
                <th class="border-table">Position</th>
                <th class="border-table">Date Registered </th>
            </tr>
            </thead>
        </table>

    </div>

</div>

<!--******Modal for edit contact**** -->
<div class="modal fade" id="confirm-cashout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content" style="height: 395px;left: -50px;top: 93px;width: 699px;">
            <div class="modal-header">

            </div>
            <div class="modal-body">
                <form role="form" class="form">
                    <input type="hidden" class="form-control" id="user-withdraw-id" readonly>
                    <input type="hidden" class="form-control" id="userId" readonly>
                    <div class="form-group">
                        <label for="account-no">Bank Account Number</label>
                        <input type="text" class="form-control" id="account-no" readonly>
                    </div>
                    <div class="form-group">
                        <label for="account-name">Account Name</label>
                        <input type="text" class="form-control" id="account-name" readonly>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary btn-cashout">Cashout</a>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function(){
        $('#downlineList').dataTable({
            "scrollCollapse": true,
            "sDom": '<"top"f>rt<"bottom"ip><"clear">',
            "bServerSide": true,
            "sAjaxSource": "ajax?type=downline-list"
        });

        $('#edit-contact').on('click',function(){
            //alert('Edit Temporary Not Available');
        });
        $('#edit-access').on('click',function(){
            //alert('Edit Temporary Not Available');
        });
    });
</script>