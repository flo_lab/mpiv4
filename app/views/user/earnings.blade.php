<link rel="stylesheet" type="text/css" href="{{Theme::asset()->usePath()->url('css/user-dashboard/earnings.css')}}">
<div class="earnings border">
    <h5 class="center">Earnings</h5>
    {{--<table class="striped highlight responsive-table">--}}
        {{--<thead>--}}
        {{--<tr>--}}
            {{--<th data-field="amount">Amount</th>--}}
            {{--<th data-field="points">Points</th>--}}
            {{--<th data-field="date">Date</th>--}}
        {{--</tr>--}}
        {{--</thead>--}}
        {{--<tbody>--}}
        {{--<tr>--}}
            {{--<td>0.00</td>--}}
            {{--<td>0</td>--}}
            {{--<td>00-Dec-2015</td>--}}
        {{--</tr>--}}
        {{--</tbody>--}}
    {{--</table>--}}
    <table id="earnings" class="display striped highlight responsive-table"  style="font-size:11px; " >
        <thead style="font-size: 14px; background: radial-gradient(#d1f1de, #b7fdd4); background: -webkit-radial-gradient(#e7eff1, #fdf9fb)">
        <tr>
            <th class="border-table">Amount</th>
            <th class="border-table">Points</th>
            <th class="border-table">Date</th>

        </tr>
        </thead>
    </table>
</div>

<div class="border earnings-total">
    <div class="row">
        <div class="col s6 text-right"><span>Total Amount :</span></div>
        <div class="col s2 text-right">{{Auth::user()->user_total_earnings}}</div>
    </div>
    <div class="row">
        <div class="col s6 text-right"><span>Total Points :</span></div>
        <div class="col s2 text-right">{{Auth::user()->user_total_points}}</div>
    </div>
    <div class="row">
        <div class="col s6 text-right"><span>Available Balance :</span></div>
        <div class="col s2 text-right">{{Auth::user()->user_total_earnings}}</div>
    </div>
</div>
<script>
    $(document).ready(function() {
//        $('#earnings').dataTable({
//            "sDom": '<"top"f>rt<"bottom"ip><"clear">',
//            "bServerSide": true,
//            "sAjaxSource": "find-reservation/ajax?type=reservation-list",
//            "bDestroy": true,
//            "scrollY": "250px",
//            "aoColumnDefs": [
//                {"bSearchable": [0], "aTargets": false}]
//        "aoColumns": [
////            { "sWidth": "null", "sClass": "center"},
//            { "sWidth": "null", "sClass": "center"},
//            { "sWidth": "null", "sClass": "center"},
//            { "sWidth": "null", "sClass": "center"},
//            { "sWidth": "null", "sClass": "center"},
//            { "sWidth": "null", "sClass": "center"},
//            { "sWidth": "null", "sClass": "center"},
//            { "sWidth": "null", "sClass": "center"},
//            { "sWidth": "null", "sClass": "center"},
//            { "sWidth": "null", "sClass": "center"},
//            { "sWidth": "null", "sClass": "center"},
//            { "sWidth": "null", "sClass": "center", "bSortable": false }
//        ]
//            ,
//        });
        var dtId='#earnings';
        var dt = $(dtId).DataTable({
            "sDom": '<"top"f>rt<"bottom"ip><"clear">',
            "pageLength": 150,
            "bInfo": false,
            "bDestroy":true,
            "bSort" : false,
            "scrollY": "375px",
            "scrollCollapse": true,
            "paging": false,
            "bFilter": false,
            "language": {
                "emptyTable":     "<span style='color:red;margin-left: 35%'>--No Data To Be Print--</span>"
            },
            "initComplete" : function () {
                $('.dataTables_scrollBody thead tr').addClass('hidden');
            }
        });
        $.ajax({
            url:"ajax?type=earning-list",
            cache:false,
            method:"POST",
            processData:false,
            contentType:false,
            dataType:"json",

            success:function(data){
                var table = $(dtId).DataTable();
                table.clear().draw();

                $.each(data.result,function(index,val){

                    dt.row.add( [
                        val[0],
                        val[1],
                        val[2]

                    ] ).draw();
                });
            },
            error:function(){

            }
        });
    });
</script>
