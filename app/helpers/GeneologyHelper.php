<?php

class GeneologyHelper {
    public static function headerStatus($headStatus)
    {
        if($headStatus==1){
            $class="fifthColor entrep";
            $stat = "Entrep";

        }else if($headStatus==2){
            $class="fourthColor bronze";
            $stat = "Bronze";

        }else if($headStatus==3){
            $class="secondColor silver";
            $stat = "Silver";

        }else if($headStatus==4){
            $class="firstColor gold";
            $stat = "Gold";
        }else if($headStatus==5){
            $class="firstColor diamond";
            $stat = "Diamond";

        }else{
            $class="diamondColor default-white";
            $stat = "Default";
        }

        return $class;
    }
}