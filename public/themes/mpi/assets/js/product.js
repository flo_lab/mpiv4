$(document).ready(function(){
    /*product*/
    $(document).ready(function(){
        $('.mont-prod-main').slick({
            slidesToShow:1,
            slidesToScroll:1,
            arrows:false,
            fade:true,
            cssEase:'linear',
            asNavFor:'.mont-prod-item',
            autoplay:false
        });

        $('.mont-prod-item').slick({
            slidesToShow:5,
            slidesToScroll:5,
            asNavFor:'.mont-prod-main',
            dots:true,
            focusOnSelect:true,
            arrows:true,
            autoplay:false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });
});
