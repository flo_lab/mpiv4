function removeZoom(){
    $('#geneology').removeClass('view80');
    $('#geneology').removeClass('view50');
    $('#geneology').removeClass('view25');
}
$(document).ready(function(){
    $('.zoom').children('button:eq(0)').click(function(){
        removeZoom();
    });
    $('.zoom').children('button:eq(1)').click(function(){
        removeZoom();
        $('#geneology').addClass('view80');
    });
    $('.zoom').children('button:eq(2)').click(function(){
        removeZoom();
        $('#geneology').addClass('view50');
    });
    $('.zoom').children('button:eq(3)').click(function(){
        removeZoom();
        $('#geneology').addClass('view25');
    });
});

$(document).ready(function(){
    $('.dealerNo').on('click',function(){
        var x = $(this).data('downline');
        var y = $(this).data('status');
        $('input[name=dealerId]').val(x);
        $('input[name=dealerStatus]').val(y);
        $('form#dealer').submit();
    });
    $('#-myaccount').on('click',function(){
        $('form#Auth').submit();
    });

    var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
    var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // At least Safari 3+: "[object HTMLElementConstructor]"
    var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
    var isIE = /*@cc_on!@*/false || !!document.documentMode;   // At least IE6

    if(isFirefox){
        $('#geneology span div').css("cssText","top:-42px");
    }

});

$(function(){
    var curDown = false,
        curYPos = 0,
        curXPos = 0;
    $('#user-dashboard').mousemove(function(m){
        if(curDown === true){
            $('#user-dashboard').scrollTop($('#user-dashboard').scrollTop() + (curYPos - m.pageY));
            $('#user-dashboard').scrollLeft($('#user-dashboard').scrollLeft() + (curXPos - m.pageX));
        }
    });

    $('#user-dashboard').mousedown(function(m){
        curDown = true;
        curYPos = m.pageY;
        curXPos = m.pageX;
    });

    $('#user-dashboard').mouseup(function(){
        curDown = false;
    });
})