$(document).ready(function() {

    $('#withdraw-status').dataTable({
        "scrollCollapse": true,
        "sDom": '<"top"f>rt<"bottom"ip><"clear">',
        "bServerSide": true,
        "sAjaxSource": "ajax?type=withdraw-list"
    });


    $('select').material_select();
    $('.modal-type').leanModal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        in_duration: 50, // Transition in duration
        out_duration: 200, // Transition out duration
        ready: function() { /*alert('Ready');*/ }, // Callback for Modal open
        complete: function() { /*alert('Closed');*/ } // Callback for Modal close
    });

    $('select[name=select-type]').change(function() {
        $('.tab-content').css('visibility', 'visible');
        if ($(this).val() == 0) {
            $('ul.tabs').tabs('select_tab', 'cash-tab');
        } else if ($(this).val() == 1) {
            $('ul.tabs').tabs('select_tab', 'bank-tab');
        } else if ($(this).val() == 2) {
            $('ul.tabs').tabs('select_tab', 'check-tab');
        }
    });

    $('#confirmWithdraw').click(function(){

        var withDrawType = $('#withdrawType').find(':selected').val();
        var account_name = $('#account_name').val();
        var account_number = $('#account_number').val();
        var formData = new FormData();
        formData.append('withdrawType',withDrawType);
        formData.append('accountName',account_name);
        formData.append('accountNo',account_number);

        if(withDrawType!=""){

            if(withDrawType==2){

                if(account_name!='' && account_number!=''){
                    var con=confirm('windraw balance?');
                    if(con==true){
                        $.ajax({
                            url:"ajax?type=withdraw",
                            cache:false,
                            method:"POST",
                            processData:false,
                            contentType:false,
                            dataType:"json",
                            data:formData,
                            success:function(data) {
                                if (data.result == true) {

                                    location.reload();
                                }
                            },
                            error:function(){

                            }
                        });
                    }
                }else{
                    alert('Please enter account name & account number');
                }

            }else{
                var con=confirm('windraw balance?');
                if(con==true){
                    $.ajax({
                        url:"ajax?type=withdraw",
                        cache:false,
                        method:"POST",
                        processData:false,
                        contentType:false,
                        dataType:"json",
                        data:formData,
                        success:function(data) {
                            if (data.result == true) {

                                location.reload();
                            }
                        },
                        error:function(){

                        }
                    });
                }
            }


        }else{
            alert('Please select withdraw type.');
        }

    });
});