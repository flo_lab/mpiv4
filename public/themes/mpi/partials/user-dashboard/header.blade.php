<div class="light-green darken-4">
    <div style="background-color: rgba(0, 0, 0, 0.1);color: #fff;padding: 0 5px;">M P I <span> - Ladder to Success</span></div>
</div>
<nav class="light-green darken-4 navbar-fixed sticky" role="navigation" style="box-shadow:none">
    <div class="nav-wrapper container">
        <a id="logo-container" href="javascript:void(0);" class="brand-logo">
            <img src="{{ URL::to('themes/mpi/assets/img/logo.png') }}" width="110" height="65">
        </a>
        <ul class="right hide-on-med-and-down">
            <li class="{{ Request::path() == 'user/home' ? 'active' : ''  }}"><a href="{{ URL::to('user/home')  }}">Home</a></li>
            {{--<li class="{{ Request::path() == 'user/profile' ? 'active' : ''  }}"><a href="{{ URL::to('user/profile')  }}">User Profile</a></li>--}}
            <li class="{{ Request::path() == 'user/geneology' ? 'active' : ''  }}"><a href="{{ URL::to('user/geneology')  }}">Geneology</a></li>
            <li class="{{ Request::path() == 'user/earnings' ? 'active' : ''  }}"><a href="{{ URL::to('user/earnings')  }}">Earning</a></li>
            <li class="{{ Request::path() == 'user/cashout' ? 'active' : ''  }}"><a href="{{ URL::to('user/cashout')  }}">Cashout</a></li>
            <li><a href="{{ URL::to('user/logout')  }}">Log-out</a></li>
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li class="{{ Request::path() == 'user/home' ? 'active' : ''  }}"><a href="{{ URL::to('user/home')  }}">Home</a></li>
            {{--<li class="{{ Request::path() == 'user/profile' ? 'active' : ''  }}"><a href="{{ URL::to('user/profile')  }}">User Profile</a></li>--}}
            <li class="{{ Request::path() == 'user/geneology' ? 'active' : ''  }}"><a href="{{ URL::to('user/geneology')  }}">Geneology</a></li>
            <li class="{{ Request::path() == 'user/earnings' ? 'active' : ''  }}"><a href="{{ URL::to('user/earnings')  }}">Earning</a></li>
            <li class="{{ Request::path() == 'user/cashout' ? 'active' : ''  }}"><a href="{{ URL::to('user/cashout')  }}">Cashout</a></li>
            <li><a href="{{ URL::to('user/logout')  }}">Log-out</a></li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">=</i></a>
    </div>
</nav>