<div class="light-green darken-4">
    <div style="background-color: rgba(0, 0, 0, 0.1);color: #fff;padding: 0 5px;">M P I <span> - Ladder to Success</span></div>
</div>
<nav class="light-green darken-4 navbar-fixed sticky" role="navigation" style="box-shadow:none">
    <div class="nav-wrapper container">
        <a id="logo-container" href="{{ URL::to('/') }}" class="brand-logo">
            <img src="{{ URL::to('themes/mpi/assets/img/logo.png') }}" width="110" height="65">
        </a>
        <ul class="right hide-on-med-and-down">
            <li class="{{ Request::path() == '/' ? 'active' : ''  }}"><a href="{{ URL::to('/')  }}">Home</a></li>
            <li class="{{ Request::path() == 'about-us' ? 'active' : ''  }}"><a href="{{ URL::to('about-us')  }}">About Us</a></li>
            <li class="{{ Request::path() == 'product' ? 'active' : ''  }}"><a href="{{ URL::to('product')  }}">Products</a></li>
            <li class="{{ Request::path() == 'marketing-plan' ? 'active' : ''  }}"><a href="{{ URL::to('marketing-plan')  }}">Marketing Plan</a></li>
            <li class="{{ Request::path() == 'contact-us' ? 'active' : ''  }}"><a href="{{ URL::to('contact-us')  }}">Contact Us</a></li>
            <li><a href="{{ URL::to('login')  }}" target="_blank">Login</a></li>
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li class="{{ Request::path() == '/' ? 'active' : ''  }}"><a href="{{ URL::to('/')  }}">Home</a></li>
            <li class="{{ Request::path() == 'about-us' ? 'active' : ''  }}"><a href="{{ URL::to('about-us')  }}">About Us</a></li>
            <li class="{{ Request::path() == 'product' ? 'active' : ''  }}"><a href="{{ URL::to('product')  }}">Products</a></li>
            <li class="{{ Request::path() == 'marketing-plan' ? 'active' : ''  }}"><a href="{{ URL::to('marketing-plan')  }}">Marketing Plan</a></li>
            <li class="{{ Request::path() == 'contact-us' ? 'active' : ''  }}"><a href="{{ URL::to('contact-us')  }}">Contact Us</a></li>
            <li><a href="{{ URL::to('login')  }}" target="_blank">Login</a></li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">=</i></a>
    </div>
</nav>