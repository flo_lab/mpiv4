<footer class="footer-copyright light-green darken-4 fixed-footer">
    <div class="container white-text">
        Designed by <a class="light-green-text text-lighten-2" href="javascript:void(0);">MPI Ninja</a>
    </div>
</footer>