<!DOCTYPE html>
<html>
    <head>
        <title>Mont Pierre International - MPI</title>
        <meta charset="utf-8">
        {{ Theme::asset()->styles() }}
        {{ Theme::asset()->scripts() }}
    </head>
    <body>
        {{ Theme::partial('header') }}
        {{ Theme::content() }}
        {{ Theme::partial('footer') }}
    </body>
</html>