<!DOCTYPE html>
<html>
<head>
    <title>{{ Theme::get('title') }}</title>
    <meta charset="utf-8">
    {{ Theme::asset()->styles() }}
    {{ Theme::asset()->scripts() }}
</head>
<body>
{{ Theme::partial('user-dashboard/header') }}
<div id="user-dashboard" style="visibility:visible;width: 1300px; margin: auto; ">
    <br><br>
    {{ Theme::content() }}
    <br><br>
</div>
</body>
{{ Theme::partial('footer') }}
</html>