
$(document).ready(function(){
	$("#contentCarousel").bind('slide.bs.carousel', function (e) {
		var slideFrom = $(this).find('.active').index();
    	var slideTo = $(e.relatedTarget).index();
    	$("#menu li").children("a").removeClass("active");
    	
	    if(slideTo==4){
	    	$('#menu li').eq(3).children("a").addClass("active");
	    }else if(slideTo==5){
	    	$('#menu li').eq(4).children("a").addClass("active");
	    }else if(slideTo==6){
	    	$('#menu li').eq(5).children("a").addClass("active");
	    }else{
	    	$('#menu li').eq(slideTo).children("a").addClass("active");
	    }
	});	

	$('.step-video').css("cssText","display:none;");

	$('.step').click(function(){
		$('.step-image').css("cssText","display:block");
		$('.step-video').css("cssText","display:none;");
		$('.step-image').attr('src',$(this).attr('data'));
	});
	$('.stepV').click(function(){
		$('.step-video').css("cssText","display:block");
		$('.step-image').css("cssText","display:none");
	});
	$('.home-slide').hover(function(){
		$(this).attr('src',$(this).attr('data'));
	});

	var default_image=$('.home-slide').attr('src');
	$('.home-slide').hover(
       function () {
          $(this).attr('src',$(this).attr('data'));
       }, 
		
       function () {
          $(this).attr('src',default_image);
       }
    );

   /* $('.product').click(function(){
    	$('.modal-body').children().remove();
    	$('.modal-body').append('<img src="'+$(this).attr('src')+'">');
    });*/

	$('.product').click(function(){
        $('.productContainer ul li').css("cssText","background: rgba(169, 68, 66, 0.59)");
        $(this).parent('li').css("cssText","background:white");
    	$('.parent-product').children().remove();
    	$('.parent-product').append('<center><img src="'+$(this).attr('src')+'"></center>');
    	pic=$(this).attr('id');
    });
});	