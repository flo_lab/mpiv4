<div class="item plan-slide-image">
	<div>
    <button class="btn btn-default step" type="button" data="{{Theme::asset()->url('img/marketing-plan/A.png')}}">
        Ways to <span class="badge">Earn</span>
    </button>
	<button class="btn btn-success step" type="button" data="{{Theme::asset()->url('img/marketing-plan/F.png')}}">
	  Step <span class="badge">1</span>
	</button>
	<button style="background-color: #B56005;border-color: #CF7C25;" class="btn btn-success step" type="button" data="{{Theme::asset()->url('img/marketing-plan/G.png')}}">
	  Step <span class="badge">2</span>
	</button>
	<button style="background-color: #949494;border-color: #747474;" class="btn btn-success step" type="button" data="{{Theme::asset()->url('img/marketing-plan/H.png')}}">
	  Step <span class="badge">3</span>
	</button>
	<button style="background-color: #EDB200;border-color: #F7BE00;" class="btn btn-success step" type="button" data="{{Theme::asset()->url('img/marketing-plan/I.png')}}">
	  Step <span class="badge">4</span>
	</button>
	<button class="btn btn-info step" type="button" data="{{Theme::asset()->url('img/marketing-plan/J.png')}}">
	  Step <span class="badge">5</span>
	</button>
	<button class="btn btn-info step" type="button" data="{{Theme::asset()->url('img/marketing-plan/K.png')}}">
	  Step <span class="badge">6</span>
	</button>
	<button class="btn btn-info step" type="button" data="{{Theme::asset()->url('img/marketing-plan/L.png')}}">
	  Step <span class="badge">7</span>
	</button>
	<button class="btn btn-primaty stepV" type="button">
	  Watch <span class="badge">100%</span>
	</button>
	</div>
	<img src="{{Theme::asset()->url('img/marketing-plan/A.png')}}" class="step-image">
	<center><video class="step-video" width="640" height="480" controls>
	  <source src="{{Theme::asset()->url('video/video.mp4')}}" type="video/mp4">
	  <source src="movie.ogg" type="video/ogg">
	  Your browser does not support the video tag.
	</video></center>
</div>