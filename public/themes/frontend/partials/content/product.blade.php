<div class="item">
	<div class="productContainer">
		<div class="parent-product">
			<center><img class="" data-toggle="modal" data-target="#myModal" src="{{Theme::asset()->url('img/products/1.png')}}"></center>
		</div>
		<ul>
            <li>
                <img class="product" data-toggle="modal" data-target="#myModal" src="{{Theme::asset()->url('img/products/1.png')}}">
                <span>Product</span>
            </li>
            <li>
                <img class="product" data-toggle="modal" data-target="#myModal" src="{{Theme::asset()->url('img/products/2.png')}}">
                <span>Product</span>
            </li>
            <li>
                <img class="product" data-toggle="modal" data-target="#myModal" src="{{Theme::asset()->url('img/products/3.png')}}">
                <span>Product</span>
            </li>
            <li>
                <img class="product" data-toggle="modal" data-target="#myModal" src="{{Theme::asset()->url('img/products/4.png')}}">
                <span>Product</span>
            </li>
			
			<li>
				<img class="product" data-toggle="modal" data-target="#myModal" src="{{Theme::asset()->url('img/products/FormatFactoryATT_1444716842089_IMG_20151013_132010.png')}}">
				<span>Product A</span>
			</li>	 
			<li>
				<img class="product" data-toggle="modal" data-target="#myModal" src="{{Theme::asset()->url('img/products/FormatFactoryATT_1444716841652_IMG_20151013_132111.png')}}">
				<span>Product Product</span>
			</li>
			<li>
				<img class="product" data-toggle="modal" data-target="#myModal" src="{{Theme::asset()->url('img/products/FormatFactoryATT_1444716841921_IMG_20151013_131536.png')}}">
				<span>Product A</span>
			</li>
			<li>
				<img class="product" data-toggle="modal" data-target="#myModal" src="{{Theme::asset()->url('img/products/FormatFactoryATT_1444728389137_IMG_20151013_132522.png')}}">
				<span>Product Product Product Product</span>
			</li>		
			<li>
				<img class="product" data-toggle="modal" data-target="#myModal" src="{{Theme::asset()->url('img/products/FormatFactoryATT_1444728397542_IMG_20151013_132501.png')}}">
				<span>Product A</span>
			</li>		
		</ul>
	</div>
</div>

<!-- Modal -->
<!-- <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Product A</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div> -->