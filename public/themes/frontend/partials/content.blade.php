<div id="contentCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
  <div class="carousel-inner" role="listbox">
      {{ Theme::partial('content/home') }}
      {{ Theme::partial('content/about') }}
      {{ Theme::partial('content/product') }}
      {{ Theme::partial('content/plan') }}
      {{ Theme::partial('content/contact') }}
      {{ Theme::partial('content/login') }}
  </div>
  <ol class="carousel-indicators">
    <li data-target="#contentCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#contentCarousel" data-slide-to="1"></li>
    <li data-target="#contentCarousel" data-slide-to="2"></li>
    <li data-target="#contentCarousel" data-slide-to="3"></li>
    <li data-target="#contentCarousel" data-slide-to="4"></li>
    <li data-target="#contentCarousel" data-slide-to="5"></li>
  </ol>
</div>