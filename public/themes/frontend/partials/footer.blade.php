<footer>
	<div id="updates">
		<h2>Updates</h2>
		<p>Lorem Ipsum est un générateur de faux textes aléatoires. Vous choisissez le nombre de paragraphes, de mots ou de listes. Vous obtenez alors un texte aléatoire que vous pourrez ensuite utiliser librement dans vos maquettes.Le texte généré est du pseudo latin et peut donner l'impression d'être du vrai texte. </p>
	</div>
	<div id="social-media">
		<h2>Share with Others</h2>
        <div class="facebook"><a href="#">Be a fan on Facebook</a></div>
        <div class="twitter"><a href="#">Follow us on Twitter</a></div>
	</div>
	<div id="testimonials">
		<h2>Testimonials</h2>
		<ul class="ls">
        	<li><a href="#">Lorem Ipsum est un générateur</a></li>
            <li><a href="#">Vous choisissez le nombre de paragraphes </a></li>
            <li><a href="#">Vous obtenez alors un texte aléatoire</a></li>
        </ul>
	</div>
	<div class="clearfix"></div>
	<div id="copyright">
	<center>
		<p>Copyright  2015. Mont Pierre International</p>
		<p>Designed by MPI Group</p>
	</center>
	</div>
</footer>