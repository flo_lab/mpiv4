<header>
  <div id="logo">
    <img src="{{Theme::asset()->url('img/header/new_logo.png')}}" width="130" height="85">
    <h1>Mont Pierre International</h1>
  </div>
  <div id="menu">
    <ul>
      <li data-target="#contentCarousel" data-slide-to="0"><a href="#home" class="active">Home</a></li>
      <li data-target="#contentCarousel" data-slide-to="1"><a href="#about-us">About Us</a></li>
      <li data-target="#contentCarousel" data-slide-to="2"><a href="#products">Products</a></li>
      <li data-target="#contentCarousel" data-slide-to="3"><a href="#marketing-plan">Marketing Plan</a></li>
      <li data-target="#contentCarousel" data-slide-to="4"><a href="#contact-us" style="display:none">Contact Us</a></li>
      <li {{--data-target="#contentCarousel" data-slide-to="5"--}}><a target="_blank" href="{{ URL::to('login') }}">Login</a></li>
    </ul>
  </div>
</header>