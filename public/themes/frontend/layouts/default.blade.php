<!DOCTYPE html>
<html>
    <head>
        <title>MPI - MontPierre International{{ Theme::get('title') }}</title>
        <meta charset="utf-8">
        <meta name="keywords" content="{{ Theme::get('keywords') }}">
        <meta name="description" content="{{ Theme::get('description') }}">
        {{ Theme::asset()->styles() }}
        {{ Theme::asset()->scripts() }}
    </head>
    <body>
        {{ Theme::partial('header') }}
        <!-- <a class="left carousel-control" href="#contentCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#contentCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a> -->
        <div class="container">
            <div id="contentCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
                <div class="carousel-inner" role="listbox">
                    {{ Theme::partial('content/home') }}
                    {{ Theme::partial('content/about') }}
                    {{ Theme::partial('content/product') }}
                    {{ Theme::partial('content/plan') }}
                </div>
                <ol class="carousel-indicators">
                    <li data-target="#contentCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#contentCarousel" data-slide-to="1"></li>
                    <li data-target="#contentCarousel" data-slide-to="2"></li>
                    <li data-target="#contentCarousel" data-slide-to="3"></li>
                    <li data-target="#contentCarousel" data-slide-to="4"></li>
                    <li data-target="#contentCarousel" data-slide-to="5"></li>
                </ol>
            </div>
            {{--{{ Theme::partial('content') }}--}}
        </div>

        {{ Theme::partial('footer') }}

        {{ Theme::asset()->container('footer')->scripts() }}
    </body>
</html>