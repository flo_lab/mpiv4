<!DOCTYPE html>
<html lang="en">
<head>
    <title>MPI</title>
    {{ Theme::partial('metadata') }}
</head>
<body style="background-color: #eaeaea" >
<div>
    {{ Theme::partial('header') }}

    <div class="container" style="width: 100%; ">
        <div style="width: 100%; margin: auto; ">
            <table>
                <tr>
                    <td valign="top">{{ Theme::partial('sidebar_menu') }}</td>
                    <td valign="top" align="left" style="width: 1075px;">

                        <div style="margin-top: 10px;">{{ Theme::content() }}</div>
                    </td>
                </tr>
            </table>
        </div>
    </div>

</div>

{{ Theme::partial('footer') }}

{{ Theme::asset()->container('footer')->scripts() }}
</div>
</body>
</html>