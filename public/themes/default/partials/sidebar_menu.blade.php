{{ HTML::style('themes/admin/assets/css/partials/partials.css'); }}
<div class="navContainer" style="margin-top: 10px; margin-left: 10px;" >
    <a href="{{ URL::to('user/logout') }}" class="{{ Request::path() == 'user/logout' ? 'active-menu' : '' }}"><div class="sideBarMenu"><i class="glyphicon glyphicon-log-out">&nbsp;</i>Logout</div></a>
    <a href="{{ URL::to('user/home') }}" class="{{ Request::path() == 'user/home' ? 'active-menu' : '' }}"><div class="sideBarMenu dropDownSidebar" id="managementSideBar"><i class="glyphicon glyphicon-home">&nbsp;</i>Home</div></a>
    <a href="{{ URL::to('user/profile') }}" class="{{ Request::path() == 'user/profile' ? 'active-menu' : '' }}"><div class="sideBarMenu dropDownSidebar" id="managementSideBar"><i class="glyphicon glyphicon-user">&nbsp;</i>Profile</div></a>
    <a href="{{ URL::to('user/access') }}" class="{{ Request::path() == 'user/access' ? 'active-menu' : '' }}"><div class="sideBarMenu dropDownSidebar" id="managementSideBar"><i class="glyphicon glyphicon-tree-deciduous">&nbsp;</i>Geneology</div></a>
    <a href="{{ URL::to('user/earnings') }}" class="{{ Request::path() == 'user/earnings' ? 'active-menu' : '' }}"><div class="sideBarMenu dropDownSidebar" id="managementSideBar"><i class="glyphicon glyphicon-usd">&nbsp;</i>Earnings</div></a>

    <a href="{{ URL::to('user/payout') }}" class="{{ Request::path() == 'user/payout' ? 'active-menu' : '' }}"><div class="sideBarMenu"><i class="glyphicon glyphicon-import">&nbsp;</i>Payout</div></a>


</div>
