{{--<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>--}}

<header>
   <div id="user-dashboard-header">
       <div class="index-header">
           <a href="{{ URL::to('/') }}">
               <img src="{{ URL::to('themes/frontend/assets/img/header/new_logo.png') }}" width="80" height="40">
           <p>Mont Pierre International</p></a>
       </div>
       <div class="index-header-info" style="display: none">
           <div>
               <a class="dealerNo" href="#" data-downline="{{Auth::User()->user_dealer_id}}" data-status="{{Auth::User()->user_status}}">{{Auth::User()->user_dealer_id}}</a>
           </div>
           <div>{{Auth::User()->user_fname.' '.Auth::User()->user_mname.' '.Auth::User()->user_lname}}</div>
               <?php
               $class='';
               if(Auth::User()->user_status==1)
                   $class="Entrep";
               else if(Auth::User()->user_status==2)
                   $class="Bronze";
               else if(Auth::User()->user_status==3)
                   $class="Silver";
               else if(Auth::User()->user_status==4)
                   $class="Gold";
               else
                   $class="Diamond";
               ?>
            <div>{{$class}}</div>
       </div>
   </div>
</header>