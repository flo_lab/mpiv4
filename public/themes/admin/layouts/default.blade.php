{{--<!DOCTYPE html>--}}
{{--<html>--}}
    {{--<head>--}}
        {{--<title>{{ Theme::get('title') }}</title>--}}
        {{--<meta charset="utf-8">--}}
        {{--<meta name="keywords" content="{{ Theme::get('keywords') }}">--}}
        {{--<meta name="description" content="{{ Theme::get('description') }}">--}}
        {{--{{ Theme::asset()->styles() }}--}}
        {{--{{ Theme::asset()->scripts() }}--}}
    {{--</head>--}}
    {{--<body>--}}
        {{--{{ Theme::partial('header') }}--}}

        {{--<div class="container">--}}
            {{--{{ Theme::content() }}--}}
        {{--</div>--}}

        {{--{{ Theme::partial('footer') }}--}}

        {{--{{ Theme::asset()->container('footer')->scripts() }}--}}
    {{--</body>--}}
{{--</html>--}}
<!DOCTYPE html>
<html lang="en">
<head>
    <title>MPI Admin</title>
    <link href="{{Theme::asset()->url('img/logopm.png')}}" rel="shortcut icon">
    {{ Theme::partial('metadata') }}
</head>
<body style="background-color: #eaeaea" >
<div>
    {{ Theme::partial('header') }}

    <div class="container" style="width: 100%; ">
        <div style="width: 1300px; margin: auto; ">
            <table>
                <tr>
                    <td valign="top">{{ Theme::partial('sidebar_menu') }}</td>
                    <td valign="top" align="left" style="width: 1075px;"><div style="margin-top: 85px;">{{ Theme::content() }}</div></td>
                </tr>
            </table>
        </div>
    </div>

</div>

{{ Theme::partial('footer') }}

{{ Theme::asset()->container('footer')->scripts() }}
</div>
</body>
</html>