/**
 * Created by user on 11/8/2015.
 */

$('header').remove();
$(document).ready(function(){

$('#contentModal').append($('#globalModal'));
$('#contentModal > #globalModal').attr('id', 'termsAndCondition');
$('#termsAndCondition .modalTitle').text('Terms and Condition');
$('#termsAndCondition .modal-footer').append($('#modalWholeContentTerms > #footerModalTerms > a'));
$('#termsAndCondition .modal-body').append($('#modalWholeContentTerms > #contentModalTerms'));

$('#termsAnchor').click(function(){
    $('#contentModal > #globalModal').attr('id', 'termsAndCondition');
    $('#iagree').trigger('click');
});

$('#iagree').click(function(){

    if($(this).is(':checked')){
        $('#iagree-btn').css("visibility","visible");
        $('#termsAndCondition').modal('show');
    }

});

$('#iagree-btn').click(function(){
    $('#termsAndCondition').modal('hide');
});

$('form#registrationForm').validate({
    rules: {
        genCode: {
            required: true,
            remote: {
                url: "registration/ajax?type=check-generated-code",
                type: "post",
                data: {
                    generatedCode: function() {
                        return $( "#genCode" ).val();
                    }
                },
                complete: function(data){
                    if( data.responseText == "true" )
                        $('#securityCode').attr('disabled', false)

                }

            }
        },
        sponsor: {
            required: true,
            remote: {
                url: "registration/ajax?type=check-user-dealer",
                type: "post",
                data: {
                    sponsorUser: function() {
                        return $( "#sponsor" ).val();
                    }
                }
            }
        },
        placement: {
            required: true,
            remote: {
                url: "registration/ajax?type=check-user-dealer",
                type: "post",
                data: {
                    placementUser: function() {
                        return $( "#placement" ).val();
                    }
                },
                complete: function(data){
                    $('select#position').val('');
                }
            }

        },
        position: {
            required: true,
            placementCheck: true
        },
        lastname: {
            required: true
        },
        firstname: {
            required: true
        },
        middlename: {
            required: true
        },
        userName: {
            required: true,
            remote: {
                url: "registration/ajax?type=check-user-name",
                type: "post",
                data: {
                    userName: function() {
                        return $( "#userName" ).val();
                    }
                }
            }
        },
        userPassword: {
            required: true,
            minlength: 8
        },
        userConfirmPassword: {
            required: true,
            minlength: 8,
            passwordMatch: true
        },
        email: {
            validEmail: true,
            remote: {
                url: "registration/ajax?type=check-user-email",
                type: "post",
                data: {
                    email: function() {
                        return $( "#email" ).val();
                    }
                }
            }
        },
        securityCode: {
            required: true,
            remote: {
                url: "registration/ajax?type=check-given-codes",
                type: "post",
                data: {
                    genCode: function() {
                        return $("#genCode").val();
                    },
                    generatedSecurityCode: function() {
                        return $("#securityCode").val();
                    }
                },
                complete: function(data){
                    if( data.responseText == "true" )
                        $('#register').attr('disabled', false);
                    else
                        $('#register').attr('disabled', true);
                }
            }
        }
    },
    messages : {
        genCode: {
            remote: 'Control number is not existed or Already used.'
        },
        securityCode: {
            remote: 'Security code not match to control number.'
        },
        sponsor: {
            remote: 'Sponsor is not existed.'
        },
        placement: {
            remote: 'Placement is not existed.'
        },
        userName: {
            remote: 'Username is already existed.'
        },
        email: {
            remote: 'Email is already used.'
        },
        position: {
            remote: 'Placement is not available.'
        }
    },
    submitHandler: function (form) {
        form.submit();
    },
    focusInvalid: false,
    debug: true,
    errorElement: 'div',
    ignore: '*:not([name])'
});


jQuery.validator.addMethod("validEmail", function(value, element) {
    return this.optional( element ) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test( value );
}, 'Please enter a valid email address.');

jQuery.validator.addMethod( 'passwordMatch', function(value, element) {
    // Check for equality with the password inputs
    if ($("#userPassword").val() != $("#userConfirmPassword").val() )
        return false;
    else
        return true;

}, "Your password do not match.");

jQuery.validator.addMethod( 'passwordMatch', function(value, element) {
    // Check for equality with the password inputs
    if ($("#userPassword").val() != $("#userConfirmPassword").val() )
        return false;
    else
        return true;

}, "Your password do not match.");

$.validator.addMethod("placementCheck", function(value, element) {
    var isSuccess = null;

    $.ajax({
        type: "POST",
        url: "registration/ajax?type=check-placement",
        data: {
            placementCheck: $('#placement').val(),
            position: $('#position').val()
        },
        async: false,
        success: function (msg) {
            isSuccess = msg === "true" ? true : false;
        }

    });

    return isSuccess;
}, "Placement is no longer available.");


$('#birthdate').datepicker({
    format: 'DD dd/mm/yyyy',
    autoclose: true,
    todayHighlight: true
}).on('change', function(){
    $(this).blur();
}).on('keypress keyup keydown', function(e){
    e.preventDefault();
});


$('#userName').on('keypress keyup', function(e) {
    if (e.which === 32)
        return false;
})

});
