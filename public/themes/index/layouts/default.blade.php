<!DOCTYPE html>
<html>
<head>
<title>MPI</title>
<meta charset="utf-8">
<meta name="keywords" content="{{ Theme::get('keywords') }}">
<meta name="description" content="{{ Theme::get('description') }}">
{{--<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>--}}
{{ Theme::asset()->styles() }}
{{ Theme::asset()->scripts() }}
</head>
<body>
{{ Theme::partial('header') }}

<div class="container">

{{ Theme::partial('global') }}
{{ Theme::content() }}

</div>

{{ Theme::partial('footer') }}


{{ Theme::asset()->container('footer')->scripts() }}
</body>
</html>
