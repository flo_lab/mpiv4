$(document).ready(function() {
//THIS IS FOR ITEM MASTER FILE

    $('.numberOnly').on('keypress', function(event){

        var regex = new RegExp("^[0-9]*$");
        validation(event, regex);

    });

    $('.alphaOnly').on('keypress', function(event){

        var regex = new RegExp("^[ A-Za-z']*$");
        validation(event, regex);

    });

    $('.alphaOnlySpecial').on('keypress', function(event){

        var regex = new RegExp("^[ A-Za-z.,\\-']*$");
        validation(event, regex);

    });

    $('.usrnmValidation').on('keypress', function(event){

        var regex = new RegExp("^[ A-Za-z0-9-_'.']*$");
        validation(event, regex);

    });

    $('.alphaNum').on('keypress', function(event){

        var regex = new RegExp("^[ A-Za-z0-9-']*$");
        validation(event, regex);

    });

    $('.alphaNumeric').on('keypress', function(event){

        var regex = new RegExp("^[ A-Za-z0-9.,\\-#@']*$");
        validation(event, regex);

    });

    function validation(event, regex) {

        if (event.charCode != 0) {

            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {

                event.preventDefault();
                return false;
            }
        }
    }

});
